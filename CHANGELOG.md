## 2.0.1 (2022-12-21) - It's alive!
### Features
- Bot stats are updatable with BotBlock API, which sends bot info to many popular bot lists

### Bug fixes
- #3 Bot stats don't update when bot is joined/kicked from guild
- #4 Docker Compose healthcheck is not working properly

### Tech stuff
- Upgraded DSharpPlus to version 4.3.0

## 2.0.0 (2022-12-18) - Kept you waiting, huh?
### Important info
- Moved project to open source
- MIT license

### Features
- New dice roll engine that is very similar to Roll20 engine
- Rewritten turn tracker functionality, now operable with UI instead of commands
- Private turn trackers, that are only visible to its creators
- Slash Commands instead of prefixes

### Bug fixes
- Each text channel now can have multiple non-private turn trackers (one per user)

### Tech stuff
- Upgraded to .NET 7
- Supports PostgreSQL and SQLite databases
- Scalable with Docker support

## 1.0.0 (2020-05-31) - Initial release
### Features
- Custom easy-to-use dice roll engine
- Simple turn tracker
- Custom prefixes support for every Discord server

### Tech stuff
- Written in .NET Core 3.1
- Supports PostgreSQL database
