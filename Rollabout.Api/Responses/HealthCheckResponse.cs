using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Rollabout.Api.Responses;

internal record HealthCheckResponse
{
    public string Status { get; }

    public TimeSpan TotalDuration { get; }

    public Dictionary<string, HealthCheckEntry> Entries { get; }

    public HealthCheckResponse(HealthReport result)
    {
        Status = result.Status.ToString();
        TotalDuration = result.TotalDuration;
        Entries = result.Entries.ToDictionary(e => e.Key, e => new HealthCheckEntry(e.Value));
    }
}

internal record HealthCheckEntry
{
    public IReadOnlyDictionary<string, object> Data { get; }

    public string Status { get; }

    public string? Description { get; }

    public TimeSpan Duration { get; }

    public string? Error { get; }

    public IEnumerable<string> Tags { get; }

    public HealthCheckEntry(HealthReportEntry entry)
    {
        Data = entry.Data;
        Status = entry.Status.ToString();
        Description = entry.Description;
        Duration = entry.Duration;
        Error = entry.Exception?.Message;
        Tags = entry.Tags;
    }
}