namespace Rollabout.Api.Responses;

/// <summary>
/// Tracker status information response.
/// </summary>
public sealed record TurnTrackerStatusResponse
{
    /// <summary>
    /// Current turn information.
    /// </summary>
    public TurnResponse? CurrentTurn { get; init; }

    /// <summary>
    /// Next turn information.
    /// </summary>
    public TurnResponse? NextTurn { get; init; }
}