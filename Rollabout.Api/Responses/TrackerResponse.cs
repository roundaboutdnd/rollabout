namespace Rollabout.Api.Responses;

/// <summary>
/// Tracker information response.
/// </summary>
public sealed record TrackerResponse
{
    /// <summary>
    /// Tracker id.
    /// </summary>
    public long? Id { get; init; }

    /// <summary>
    /// Chat id.
    /// </summary>
    public ulong ChatId { get; init; }

    /// <summary>
    /// User id.
    /// </summary>
    public ulong UserId { get; init; }

    /// <summary>
    /// Message id.
    /// </summary>
    public ulong? MessageId { get; init; }

    /// <summary>
    /// Whether this tracker is private.
    /// </summary>
    public bool? Private { get; init; }
}