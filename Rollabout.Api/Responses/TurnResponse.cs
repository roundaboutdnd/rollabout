namespace Rollabout.Api.Responses;

/// <summary>
/// Turn information response
/// </summary>
public sealed record TurnResponse
{
    /// <summary>
    /// Turn id.
    /// </summary>
    public long Id { get; init; }

    /// <summary>
    /// Tracker id.
    /// </summary>
    public long TrackerId { get; init; }

    /// <summary>
    /// User id.
    /// </summary>
    public ulong UserId { get; init; }

    /// <summary>
    /// Turn name.
    /// </summary>
    public string? Name { get; init; }

    /// <summary>
    /// Numeric value for turn placement in tracker.
    /// </summary>
    public double? Value { get; init; }
}