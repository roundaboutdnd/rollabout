namespace Rollabout.Api.Responses;

/// <summary>
/// Evaluated coin flip response.
/// </summary>
public sealed record FlipCoinsResponse
{
    /// <summary>
    /// Number of coins.
    /// </summary>
    public int CoinsCount { get; init; }

    /// <summary>
    /// Number of "heads" results.
    /// </summary>
    public int HeadsCount { get; init; }

    /// <summary>
    /// Number of "tails" results.
    /// </summary>
    public int TailsCount { get; init; }
}