namespace Rollabout.Api.Responses;

/// <summary>
/// Evaluated expression response.
/// </summary>
public sealed record ExpressionResponse
{
    /// <summary>
    /// Original expression string.
    /// </summary>
    public string OriginalString { get; init; } = null!;

    /// <summary>
    /// Evaluated result string.
    /// </summary>
    public string ResultString { get; init; } = null!;

    /// <summary>
    /// Evaluated result value.
    /// </summary>
    public double ResultValue { get; init; }

    /// <summary>
    /// Whether the result has target number successes.
    /// </summary>
    public bool IsTargetNumberResult { get; init; }

    /// <summary>
    /// Whether the result has match successes.
    /// </summary>
    public bool IsMatchResult { get; init; }

    /// <summary>
    /// Additional text for messages.
    /// </summary>
    public string? ResultAdditionalText { get; init; }
}