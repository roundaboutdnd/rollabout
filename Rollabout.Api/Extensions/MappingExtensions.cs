using Rollabout.Api.Responses;
using Rollabout.Contracts.Entities;

namespace Rollabout.Api.Extensions;

internal static class MappingExtensions
{
    public static T? Map<T>(object? entity) where T : class => entity switch
    {
        Expression expression => expression.Map() as T,
        CoinFlips coinFlips => coinFlips.Map() as T,
        Turn turn => turn.Map() as T,
        IEnumerable<Turn> turns => turns.Select(Map) as T,
        TurnTracker tracker => tracker.Map() as T,
        TurnTrackerStatus trackerStatus => trackerStatus.Map() as T,
        _ => default
    };

    private static ExpressionResponse? Map(this Expression? expression)
    {
        if (expression == null)
            return default;

        return new ExpressionResponse
        {
            OriginalString = expression.OriginalString,
            ResultString = expression.ResultString,
            ResultValue = expression.ResultValue,
            ResultAdditionalText = expression.ResultAdditionalText,
            IsMatchResult = expression.IsMatchResult,
            IsTargetNumberResult = expression.IsTargetNumberResult
        };
    }

    private static FlipCoinsResponse? Map(this CoinFlips? coinFlips)
    {
        if (coinFlips == null)
            return default;

        return new FlipCoinsResponse
        {
            CoinsCount = coinFlips.CoinsCount,
            HeadsCount = coinFlips.HeadsCount,
            TailsCount = coinFlips.TailsCount
        };
    }

    private static TurnResponse? Map(this Turn? turn)
    {
        if (turn == null)
            return default;

        return new TurnResponse
        {
            Id = turn.Id,
            TrackerId = turn.TrackerId,
            UserId = turn.UserId,
            Name = turn.Name,
            Value = turn.Value
        };
    }

    private static TrackerResponse? Map(this TurnTracker? tracker)
    {
        if (tracker == null)
            return default;

        return new TrackerResponse
        {
            Id = tracker.Id,
            ChatId = tracker.ChatId,
            UserId = tracker.UserId,
            MessageId = tracker.MessageId,
            Private = tracker.Private
        };
    }

    private static TurnTrackerStatusResponse? Map(this TurnTrackerStatus? trackerStatus)
    {
        if (trackerStatus == null)
            return default;

        return new TurnTrackerStatusResponse
        {
            CurrentTurn = trackerStatus.CurrentTurn.Map(),
            NextTurn = trackerStatus.NextTurn.Map()
        };
    }
}