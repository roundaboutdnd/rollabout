using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Rollabout.Api.HealthChecks;

internal sealed class ApiHealthCheck : IHealthCheck
{
    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context,
        CancellationToken cancellationToken = default)
    {
        using var httpClient = new HttpClient();
        try
        {
            var requestUri = Environment.GetEnvironmentVariable("ASPNETCORE_URLS");
            if (string.IsNullOrEmpty(requestUri) || requestUri == "http://+:80")
            {
                requestUri = "http://localhost";
            }

            var response = await httpClient.GetAsync(requestUri, cancellationToken);

            response.EnsureSuccessStatusCode();
        }
        catch (Exception ex)
        {
            return HealthCheckResult.Unhealthy(ex.Message, ex);
        }

        return HealthCheckResult.Healthy();
    }
}