using System.ComponentModel.DataAnnotations;

namespace Rollabout.Api.Requests;

/// <summary>
/// Request for turns bulk deletion in a tracker.
/// </summary>
public sealed record BulkDeleteTurnsRequest : AuthorizedRequest
{
    /// <summary>
    /// A collection of turn ids.
    /// </summary>
    [Required]
    public required IEnumerable<long> TurnIds { get; init; } = null!;
}