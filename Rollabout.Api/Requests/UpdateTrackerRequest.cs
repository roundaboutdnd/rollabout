namespace Rollabout.Api.Requests;

/// <summary>
/// Request for tracker information update.
/// </summary>
public sealed record UpdateTrackerRequest : AuthorizedRequest
{
    /// <summary>
    /// New current turn id.
    /// </summary>
    public long? NewCurrentTurnId { get; init; }

    /// <summary>
    /// New next turn id.
    /// </summary>
    public long? NewNextTurnId { get; init; }

    /// <summary>
    /// New message id.
    /// </summary>
    public ulong? NewMessageId { get; init; }
}