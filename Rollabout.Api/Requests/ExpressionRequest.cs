using System.ComponentModel.DataAnnotations;

namespace Rollabout.Api.Requests;

/// <summary>
/// Request for expression evaluation.
/// </summary>
public sealed record ExpressionRequest
{
    /// <summary>
    /// Expression string.
    /// </summary>
    [Required]
    public required string Expression { get; init; } = null!;

    /// <summary>
    /// Whether to use ANSI formatting.
    /// </summary>
    public bool UseAnsiFormatting { get; init; } = false;
}