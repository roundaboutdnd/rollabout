using System.ComponentModel.DataAnnotations;

namespace Rollabout.Api.Requests;

/// <summary>
/// Request for turn creation in a tracker.
/// </summary>
public sealed record CreateTurnRequest : AuthorizedRequest
{
    /// <summary>
    /// Turn name.
    /// </summary>
    [Required]
    public required string Name { get; init; } = null!;

    /// <summary>
    /// Numeric value for turn placement in tracker.
    /// </summary>
    [Required]
    public double Value { get; init; }
}