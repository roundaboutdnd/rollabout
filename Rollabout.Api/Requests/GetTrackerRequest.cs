using System.ComponentModel.DataAnnotations;

namespace Rollabout.Api.Requests;

/// <summary>
/// Request for tracker information retrieving.
/// </summary>
public sealed record GetTrackerRequest : AuthorizedRequest
{
    /// <summary>
    /// Chat id.
    /// </summary>
    [Required]
    public required ulong ChatId { get; init; }
}