using System.ComponentModel.DataAnnotations;

namespace Rollabout.Api.Requests;

/// <summary>
/// Base request with auth requirements.
/// </summary>
public record AuthorizedRequest
{
    /// <summary>
    /// User id.
    /// </summary>
    [Required]
    public required ulong UserId { get; init; }
}