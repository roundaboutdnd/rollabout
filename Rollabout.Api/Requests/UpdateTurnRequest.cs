namespace Rollabout.Api.Requests;

/// <summary>
/// Request for turn information update.
/// </summary>
public sealed record UpdateTurnRequest : AuthorizedRequest
{
    /// <summary>
    /// Tracker id.
    /// </summary>
    public long TrackerId { get; init; }

    /// <summary>
    /// New turn name.
    /// </summary>
    public string? NewName { get; init; }

    /// <summary>
    /// New numeric value for turn placement in a tracker.
    /// </summary>
    public double? NewValue { get; init; }
}