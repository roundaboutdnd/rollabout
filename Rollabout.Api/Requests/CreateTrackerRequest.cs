using System.ComponentModel.DataAnnotations;

namespace Rollabout.Api.Requests;

/// <summary>
/// Request for tracker creation.
/// </summary>
public sealed record CreateTrackerRequest : AuthorizedRequest
{
    /// <summary>
    /// Chat id.
    /// </summary>
    [Required]
    public required ulong ChatId { get; init; }

    /// <summary>
    /// Whether to create a private tracker.
    /// </summary>
    public bool Private { get; init; } = false;
}