using System.Net.Mime;
using System.Reflection;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.Extensions.Logging.Console;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Rollabout.Api.Controllers;
using Rollabout.Api.HealthChecks;
using Rollabout.Api.Responses;
using Rollabout.Dal.Extensions;
using Rollabout.Engine.Extensions;

#pragma warning disable ASP0000

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddRollaboutEngineServices();
builder.Services.AddRollaboutDatabaseServices(builder.Configuration);

var hasConnectionString = builder.Configuration.GetSection("ConnectionStrings").GetChildren().Any();
if (hasConnectionString)
{
    builder.Services.AddAuthentication().AddJwtBearer(o =>
    {
        o.TokenValidationParameters = new TokenValidationParameters
        {
            ValidAudiences = builder.Configuration.GetSection("Authentication:Schemes:Bearer:ValidAudiences")
                .GetChildren()
                .Select(s => s.Value),
            ValidIssuer = builder.Configuration["Authentication:Schemes:Bearer:ValidIssuer"],
            IssuerSigningKeys = builder.Configuration.GetSection("Authentication:Schemes:Bearer:SigningKeys")
                .GetChildren().Select(s =>
                    new SymmetricSecurityKey(new HMACSHA256(Convert.FromBase64String(s.Get<string>() ?? string.Empty))
                        .Key))
        };
    });
}

builder.Services.AddHealthChecks().AddCheck<ApiHealthCheck>("api");
builder.Services.AddControllers(o =>
{
    o.Conventions.Add(new HideDatabaseControllersConvention(builder.Configuration));
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(o =>
{
    o.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Rollabout API",
        Description = "REST API for Rollabout services."
    });

    o.IncludeXmlComments(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
        $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));

    if (!hasConnectionString)
        return;

    o.AddSecurityDefinition(JwtBearerDefaults.AuthenticationScheme, new OpenApiSecurityScheme
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        In = ParameterLocation.Header,
        Scheme = JwtBearerDefaults.AuthenticationScheme,
        BearerFormat = "JWT"
    });

    o.OperationFilter<AuthorizationRequirementOperationFilter>();
});

builder.Services.AddHttpLogging(o =>
{
    o.LoggingFields = HttpLoggingFields.RequestPath | HttpLoggingFields.RequestMethod |
                      HttpLoggingFields.RequestBody | HttpLoggingFields.RequestQuery |
                      HttpLoggingFields.ResponseStatusCode | HttpLoggingFields.ResponseBody;
});

builder.Logging.AddSimpleConsole(options =>
{
    options.ColorBehavior = LoggerColorBehavior.Enabled;
    options.SingleLine = true;
    options.TimestampFormat = "yyyy-MM-ddTHH:mm:ss.fffK ";
    options.UseUtcTimestamp = true;
});

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(o =>
{
    o.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
    o.RoutePrefix = string.Empty;
});

app.UseHttpLogging();
app.UseAuthentication();
app.UseAuthorization();
app.UseHealthChecks("/healthcheck", new HealthCheckOptions
{
    ResponseWriter = async (c, r) =>
    {
        c.Response.ContentType = MediaTypeNames.Application.Json;
        await c.Response.WriteAsJsonAsync(new HealthCheckResponse(r));
    }
});
app.MapControllers();

app.Run();