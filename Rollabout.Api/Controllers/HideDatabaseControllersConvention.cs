using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace Rollabout.Api.Controllers;

internal sealed class HideDatabaseControllersConvention : IActionModelConvention
{
    private readonly IConfiguration _configuration;

    public HideDatabaseControllersConvention(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void Apply(ActionModel action)
    {
        if (action.Controller.ControllerType == typeof(TurnTrackerController) &&
            !_configuration.GetSection("ConnectionStrings").GetChildren().Any())
        {
            action.ApiExplorer.IsVisible = false;
        }
    }
}