using Microsoft.AspNetCore.Mvc;
using Rollabout.Api.Extensions;
using UnauthorizedAccessException = Rollabout.Contracts.Exceptions.UnauthorizedAccessException;

#pragma warning disable CS1591

namespace Rollabout.Api.Controllers;

/// <summary>
/// Base class for API controllers.
/// </summary>
[ApiController]
[ApiExplorerSettings(GroupName = "v1")]
public abstract class BaseController : ControllerBase
{
    protected IActionResult CreateResponse<T>(Func<object?> func, int statusCode = StatusCodes.Status200OK)
        where T : class
    {
        T? response;
        try
        {
            response = MappingExtensions.Map<T>(func());
        }
        catch (NotImplementedException)
        {
            return NotFound();
        }
        catch (UnauthorizedAccessException ex)
        {
            return Unauthorized(ProblemDetailsFactory.CreateProblemDetails(HttpContext,
                StatusCodes.Status401Unauthorized, null, ex.GetType().Name, ex.Message, ex.Source));
        }
        catch (Exception ex)
        {
            return Problem(ex.Message, ex.Source, StatusCodes.Status500InternalServerError, type: ex.GetType().Name);
        }

        return response == null
            ? NotFound()
            : StatusCode(statusCode, response);
    }

    protected async Task<IActionResult> CreateResponseAsync<T>(Func<Task<object?>> func,
        int statusCode = StatusCodes.Status200OK) where T : class
    {
        T? response;
        try
        {
            response = MappingExtensions.Map<T>(await func());
        }
        catch (NotImplementedException)
        {
            return NotFound();
        }
        catch (Exception ex)
        {
            return Problem(ex.Message, ex.Source, StatusCodes.Status500InternalServerError, type: ex.GetType().Name);
        }

        return response == null
            ? NotFound()
            : StatusCode(statusCode, response);
    }

    protected async Task<IActionResult> CreateResponseAsync(Func<Task> func, int statusCode = StatusCodes.Status200OK)
    {
        try
        {
            await func();
        }
        catch (NotImplementedException)
        {
            return NotFound();
        }
        catch (Exception ex)
        {
            return Problem(ex.Message, ex.Source, StatusCodes.Status500InternalServerError, type: ex.GetType().Name);
        }

        return StatusCode(statusCode);
    }
}