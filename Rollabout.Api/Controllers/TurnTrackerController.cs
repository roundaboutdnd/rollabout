using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rollabout.Api.Requests;
using Rollabout.Api.Responses;
using Rollabout.Contracts.Entities;
using Rollabout.Contracts.Handlers;
using Swashbuckle.AspNetCore.Annotations;

namespace Rollabout.Api.Controllers;

/// <summary>
/// Methods for turn tracker management.
/// </summary>
[Authorize]
[Route("v1/[controller]")]
[Consumes(MediaTypeNames.Application.Json)]
[Produces(MediaTypeNames.Application.Json)]
[SwaggerResponse(StatusCodes.Status401Unauthorized, type: typeof(UnauthorizedResult))]
[SwaggerResponse(StatusCodes.Status404NotFound, type: typeof(NotFoundResult))]
public class TurnTrackerController : BaseController
{
    private readonly ITurnTrackerHandler _turnTrackerHandler;

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="turnTrackerHandler">Turn tracker handler</param>
    public TurnTrackerController(ITurnTrackerHandler turnTrackerHandler)
    {
        _turnTrackerHandler = turnTrackerHandler;
    }

    /// <summary>
    /// Gets a turn tracker by id.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <returns>Turn tracker information.</returns>
    [HttpGet("{trackerId:long}")]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(TrackerResponse))]
    public async Task<IActionResult> GetTrackerAsync([FromRoute] long trackerId) =>
        await CreateResponseAsync<TrackerResponse>(async () =>
            await _turnTrackerHandler.GetTrackerAsync(trackerId));

    /// <summary>
    /// Gets a turn tracker by chat and user ids.
    /// </summary>
    /// <param name="request">Request for tracker information retrieval</param>
    /// <returns>Turn tracker information.</returns>
    [HttpGet]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(TrackerResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, type: typeof(BadRequestResult))]
    public async Task<IActionResult> GetTrackerAsync([FromQuery] GetTrackerRequest? request = default) =>
        request == null
            ? BadRequest()
            : await CreateResponseAsync<TrackerResponse>(async () =>
                await _turnTrackerHandler.GetTrackerAsync(request.ChatId, request.UserId));

    /// <summary>
    /// Gets turns from a turn tracker.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <param name="userId">User id</param>
    /// <returns>A collection of turn information objects.</returns>
    [HttpGet("{trackerId:long}/Turns")]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(IEnumerable<TurnResponse>))]
    public async Task<IActionResult> GetTurnsAsync([FromRoute] long trackerId, [FromQuery] ulong? userId = default) =>
        await CreateResponseAsync<IEnumerable<TurnResponse>>(async () =>
            await _turnTrackerHandler.GetTurnsAsync(trackerId, userId));

    /// <summary>
    /// Gets turn tracker status.
    /// </summary>
    /// <param name="trackerId">Tracker id.</param>
    /// <returns>Turn tracker status.</returns>
    [HttpGet("{trackerId:long}/Status")]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(TurnTrackerStatusResponse))]
    public async Task<IActionResult> GetTrackerStatusAsync([FromRoute] long trackerId) =>
        await CreateResponseAsync<TurnTrackerStatusResponse>(async () =>
            await _turnTrackerHandler.GetTrackerStatusAsync(trackerId));

    /// <summary>
    /// Creates a turn tracker.
    /// </summary>
    /// <param name="request">Create tracker request.</param>
    /// <returns>Created turn tracker information.</returns>
    [HttpPost]
    [SwaggerResponse(StatusCodes.Status201Created, type: typeof(TrackerResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, type: typeof(BadRequestResult))]
    public async Task<IActionResult> CreateTrackerAsync([FromBody] CreateTrackerRequest? request = default) =>
        request == null
            ? BadRequest()
            : await CreateResponseAsync<TrackerResponse>(async () =>
                    await _turnTrackerHandler.CreateTrackerAsync(new TurnTracker
                    {
                        ChatId = request.ChatId,
                        UserId = request.UserId,
                        Private = request.Private
                    }),
                StatusCodes.Status201Created);

    /// <summary>
    /// Creates a turn in a turn tracker.
    /// </summary>
    /// <param name="trackerId">Tracker id.</param>
    /// <param name="request">Create turn request.</param>
    /// <returns>Created turn information.</returns>
    [HttpPost("{trackerId:long}/[action]")]
    [SwaggerResponse(StatusCodes.Status201Created, type: typeof(TurnResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, type: typeof(BadRequestResult))]
    public async Task<IActionResult> CreateTurnAsync([FromRoute] long trackerId,
        [FromBody] CreateTurnRequest? request = default) => request == null
        ? BadRequest()
        : await CreateResponseAsync<TurnResponse>(async () => await _turnTrackerHandler.CreateTurnAsync(
                new Turn
                {
                    TrackerId = trackerId,
                    UserId = request.UserId,
                    Name = request.Name,
                    Value = request.Value
                }),
            StatusCodes.Status201Created);

    /// <summary>
    /// Starts a turn tracker.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <param name="request">Authorized request</param>
    /// <returns>Turn tracker status information.</returns>
    [HttpPost("{trackerId:long}/Start")]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(TurnTrackerStatusResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, type: typeof(BadRequestResult))]
    public async Task<IActionResult> StartTrackerAsync([FromRoute] long trackerId,
        [FromBody] AuthorizedRequest? request = default) => request == null
        ? BadRequest()
        : await CreateResponseAsync<TurnTrackerStatusResponse>(async () =>
            await _turnTrackerHandler.StartTrackerAsync(trackerId, request.UserId));

    /// <summary>
    /// Moves to next turn in a turn tracker.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <param name="request">Authorized request</param>
    /// <returns>Turn tracker status information.</returns>
    [HttpPost("{trackerId:long}/[action]")]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(TurnTrackerStatusResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, type: typeof(BadRequestResult))]
    public async Task<IActionResult> MoveToNextTurnAsync([FromRoute] long trackerId,
        [FromBody] AuthorizedRequest? request = default) => request == null
        ? BadRequest()
        : await CreateResponseAsync<TurnTrackerStatusResponse>(async () =>
            await _turnTrackerHandler.MoveToNextTurnAsync(trackerId, request.UserId));

    /// <summary>
    /// Stops a turn tracker.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <param name="request">Authorized request</param>
    /// <returns></returns>
    [HttpPost("{trackerId:long}/Stop")]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(EmptyResult))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, type: typeof(BadRequestResult))]
    public async Task<IActionResult> StopTrackerAsync([FromRoute] long trackerId,
        [FromBody] AuthorizedRequest? request = default) => request == null
        ? BadRequest()
        : await CreateResponseAsync(() => _turnTrackerHandler.StopTrackerAsync(trackerId, request.UserId));

    /// <summary>
    /// Updates specified turn tracker's information.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <param name="request">Update tracker request</param>
    /// <returns></returns>
    [HttpPut("{trackerId:long}")]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(EmptyResult))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, type: typeof(BadRequestResult))]
    public async Task<IActionResult> UpdateTrackerAsync([FromRoute] long trackerId,
        [FromBody] UpdateTrackerRequest? request = default) => request == null
        ? BadRequest()
        : await CreateResponseAsync(() => _turnTrackerHandler.UpdateTrackerAsync(
            new TurnTracker
            {
                Id = trackerId,
                UserId = request.UserId,
                CurrentTurnId = request.NewCurrentTurnId,
                NextTurnId = request.NewNextTurnId,
                MessageId = request.NewMessageId
            }));

    /// <summary>
    /// Updates specified turn's information.
    /// </summary>
    /// <param name="turnId">Turn id</param>
    /// <param name="request">Update turn request</param>
    /// <returns></returns>
    [HttpPut("Turns/{turnId:long}")]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(EmptyResult))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, type: typeof(BadRequestResult))]
    public async Task<IActionResult> UpdateTurnAsync([FromRoute] long turnId,
        [FromBody] UpdateTurnRequest? request = default) => request == null
        ? BadRequest()
        : await CreateResponseAsync(() => _turnTrackerHandler.UpdateTurnAsync(
            new Turn
            {
                Id = turnId,
                TrackerId = request.TrackerId,
                UserId = request.UserId,
                Name = request.NewName,
                Value = request.NewValue
            }));

    /// <summary>
    /// Deletes a collection of turns from a turn tracker.
    /// </summary>
    /// <param name="request">Delete turns request</param>
    /// <returns></returns>
    [HttpDelete("Turns")]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(EmptyResult))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, type: typeof(BadRequestResult))]
    public async Task<IActionResult> BulkDeleteTurnsAsync([FromQuery] BulkDeleteTurnsRequest? request = default) =>
        request == null
            ? BadRequest()
            : await CreateResponseAsync(() =>
                _turnTrackerHandler.BulkDeleteTurnsAsync(request.UserId, request.TurnIds));

    /// <summary>
    /// Deletes a turn tracker.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <param name="userId">User id</param>
    /// <returns></returns>
    [HttpDelete("{trackerId:long}")]
    [SwaggerResponse(StatusCodes.Status204NoContent, type: typeof(NoContentResult))]
    public async Task<IActionResult> DeleteTrackerAsync([FromRoute] long trackerId,
        [FromQuery] ulong? userId = default) => userId.HasValue
        ? await CreateResponseAsync(() => _turnTrackerHandler.DeleteTrackerAsync(trackerId, userId.Value),
            StatusCodes.Status204NoContent)
        : BadRequest();
}