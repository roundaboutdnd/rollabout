using System.Net.Mime;
using Microsoft.AspNetCore.Mvc;
using Rollabout.Api.Requests;
using Rollabout.Api.Responses;
using Rollabout.Contracts.Entities;
using Rollabout.Contracts.Handlers;
using Swashbuckle.AspNetCore.Annotations;

namespace Rollabout.Api.Controllers;

/// <summary>
/// Methods for expression evaluation.
/// </summary>
[Route("v1/[controller]/[action]")]
[Consumes(MediaTypeNames.Application.Json)]
[Produces(MediaTypeNames.Application.Json)]
public class ExpressionController : BaseController
{
    private readonly IExpressionHandler _expressionHandler;

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="expressionHandler">Expression evaluation handler</param>
    public ExpressionController(IExpressionHandler expressionHandler)
    {
        _expressionHandler = expressionHandler;
    }

#if DEBUG
    /// <summary>
    /// Tests an expression without any text formatting.
    /// </summary>
    /// <param name="expression">An expression string for evaluation</param>
    /// <returns>An evaluated expression.</returns>
    [HttpGet]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ExpressionResponse))]
    public async Task<IActionResult> TestExpressionAsync([FromQuery] string expression)
    {
        if (string.IsNullOrEmpty(expression))
            return BadRequest();

        var request = new ExpressionRequest { Expression = expression };

        return await EvaluateAsync(request);
    }
#endif

    /// <summary>
    /// Flips a coin X times.
    /// </summary>
    /// <param name="coinsCount">Flip coins count</param>
    /// <returns>Coin flip result.</returns>
    [HttpGet]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(FlipCoinsResponse))]
    public async Task<IActionResult> FlipCoinsAsync([FromQuery] int coinsCount = 1) =>
        await CreateResponseAsync<FlipCoinsResponse>(async () =>
            await _expressionHandler.FlipCoinsAsync(new CoinFlips { CoinsCount = coinsCount }));

    /// <summary>
    /// Evaluates an expression.
    /// </summary>
    /// <param name="request">Evaluation request</param>
    /// <returns>An evaluated expression.</returns>
    [HttpPost]
    [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ExpressionResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, type: typeof(BadRequestResult))]
    public async Task<IActionResult> EvaluateAsync([FromBody] ExpressionRequest? request = default)
    {
        return request == default
            ? BadRequest()
            : await CreateResponseAsync<ExpressionResponse>(async () => await _expressionHandler.EvaluateAsync(
                new Expression
                {
                    OriginalString = request.Expression,
                    UseAnsiFormatting = request.UseAnsiFormatting
                }));
    }
}