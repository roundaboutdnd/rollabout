#!/usr/bin/env bash
main(){
  set -eo pipefail

  [ -n "$Authentication__Schemes__Bearer__SigningKeys__0" ] || die "Authentication__Schemes__Bearer__SigningKeys__0 environment variable is not set."

  # number of seconds to expire token. default 1y
  expire_seconds="${JWT_EXPIRATION_IN_SECONDS:-31536000}"

  # pass JWT_SECRET_BASE64_ENCODED as true if secret is base64 encoded
  ${JWT_SECRET_BASE64_ENCODED:-true} && \
    JWT_SECRET=$(printf %s "$Authentication__Schemes__Bearer__SigningKeys__0" | base64 --decode)

  header='{
    "alg": "HS256",
  	"typ": "JWT"
  }'

  payload="{
    \"aud\": \"Rollabout\",
    \"nbf\": $(($(date +%s)-1)),
    \"exp\": $(($(date +%s)+expire_seconds)),
    \"iat\": $(date +%s),
    \"iss\": \"Rollabout.Api\"
  }"

  header_base64=$(printf %s "$header" | base64_urlencode)
  payload_base64=$(printf %s "$payload" | base64_urlencode)
  signed_content="${header_base64}.${payload_base64}"
  signature=$(printf %s "$signed_content" | openssl dgst -binary -sha256 -hmac "$JWT_SECRET" | base64_urlencode)

  log "generated JWT token. expires in $expire_seconds seconds -->\\n"
  printf '%s\n' "${signed_content}.${signature}"
}

base64_urlencode() { openssl enc -base64 -A | tr '+/' '-_' | tr -d '='; }
readonly __entry=$(basename "$0")
log(){ echo -e "$__entry: $*" >&2; }
die(){ log "$*"; exit 1; }
main "$@"