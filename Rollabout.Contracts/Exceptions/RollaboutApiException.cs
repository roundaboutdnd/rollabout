using System;
using Rollabout.Contracts.Entities;

namespace Rollabout.Contracts.Exceptions;

/// <summary>
/// An exception thrown by Rollabout API.
/// </summary>
public sealed class RollaboutApiException : Exception
{
    /// <summary>
    /// Error response.
    /// </summary>
    public ApiError Error { get; }

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="error">API error response</param>
    public RollaboutApiException(ApiError error)
    {
        Error = error;
    }
}