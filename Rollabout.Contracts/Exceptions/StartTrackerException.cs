using System;

namespace Rollabout.Contracts.Exceptions;

/// <summary>
/// An exception thrown when tracker starts with insufficient number of turns.
/// </summary>
public sealed class StartTrackerException : Exception
{
    /// <summary>
    /// Tracker id.
    /// </summary>
    public long TrackerId { get; }

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    public StartTrackerException(long trackerId)
        : base("You can't start a turn tracker with less than 2 turns in it.")
    {
        TrackerId = trackerId;
    }
}