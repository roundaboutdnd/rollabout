using System;

namespace Rollabout.Contracts.Exceptions;

/// <summary>
/// An exception thrown when someone tries to access auth restricted services.
/// </summary>
public sealed class UnauthorizedAccessException : Exception
{
    private const string ErrorMessage = "You are not allowed to perform this action.";

    /// <summary>
    /// User id.
    /// </summary>
    public ulong UserId { get; }

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="userId">User id</param>
    public UnauthorizedAccessException(ulong userId)
        : base(ErrorMessage)
    {
        UserId = userId;
    }

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="userId">User id</param>
    public UnauthorizedAccessException(string userId)
        : base(ErrorMessage)
    {
        UserId = ulong.Parse(userId);
    }
}