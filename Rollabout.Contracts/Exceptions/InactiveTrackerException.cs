using System;

namespace Rollabout.Contracts.Exceptions;

/// <summary>
/// An exception thrown when tracker is inactive, but user tries to execute a forbidden operation.
/// </summary>
public sealed class InactiveTrackerException : Exception
{
    /// <summary>
    /// Tracker id.
    /// </summary>
    public long TrackerId { get; }

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    public InactiveTrackerException(long trackerId)
        : base("This tracker is not in active state.")
    {
        TrackerId = trackerId;
    }
}