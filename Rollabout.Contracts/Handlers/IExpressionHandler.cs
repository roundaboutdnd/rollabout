using System.Threading.Tasks;
using Rollabout.Contracts.Entities;

namespace Rollabout.Contracts.Handlers;

/// <summary>
/// Methods for expression handling.
/// </summary>
public interface IExpressionHandler
{
    /// <summary>
    /// Evaluates expression.
    /// </summary>
    /// <param name="expression">Expression string with additional parameters</param>
    /// <returns>Evaluated expression result</returns>
    Task<Expression> EvaluateAsync(Expression expression);

    /// <summary>
    /// Flips X number of coins.
    /// </summary>
    /// <param name="coinFlips">Coins count</param>
    /// <returns>Evaluated flip coin result</returns>
    Task<CoinFlips> FlipCoinsAsync(CoinFlips coinFlips);
}