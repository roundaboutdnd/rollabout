using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Rollabout.Contracts.Entities;

namespace Rollabout.Contracts.Handlers;

/// <summary>
/// Methods for turn tracker handling.
/// </summary>
public interface ITurnTrackerHandler
{
    /// <summary>
    /// Gets a turn tracker by id.
    /// </summary>
    /// <param name="trackerId">Turn tracker id</param>
    /// <returns>Turn tracker information</returns>
    Task<TurnTracker> GetTrackerAsync(long trackerId);

    /// <summary>
    /// Gets a turn tracker by chat and user.
    /// </summary>
    /// <param name="chatId">Chat id</param>
    /// <param name="userId">User id</param>
    /// <returns>Turn tracker information</returns>
    Task<TurnTracker?> GetTrackerAsync(ulong chatId, ulong userId);

    /// <summary>
    /// Gets a collection of turns from a turn tracker.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <param name="userId">User id</param>
    /// <returns>A collection of turns.</returns>
    Task<IEnumerable<Turn>> GetTurnsAsync(long trackerId, ulong? userId = default);

    /// <summary>
    /// Gets turn tracker status information.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <returns>Turn tracker status information.</returns>
    Task<TurnTrackerStatus?> GetTrackerStatusAsync(long trackerId);

    /// <summary>
    /// Creates a new turn tracker.
    /// </summary>
    /// <param name="tracker">New turn tracker parameters</param>
    /// <returns>Created turn tracker information</returns>
    Task<TurnTracker> CreateTrackerAsync(TurnTracker tracker);

    /// <summary>
    /// Adds a new turn to tracker.
    /// </summary>
    /// <param name="turn">New turn parameters</param>
    /// <returns>Created turn information.</returns>
    Task<Turn> CreateTurnAsync(Turn turn);

    /// <summary>
    /// Starts a turn tracker.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <param name="userId">User id</param>
    /// <returns>Turn tracker status information.</returns>
    Task<TurnTrackerStatus> StartTrackerAsync(long trackerId, ulong userId);

    /// <summary>
    /// Moves to a next turn in tracker.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <param name="userId">User id</param>
    /// <returns>Turn tracker status information.</returns>
    Task<TurnTrackerStatus> MoveToNextTurnAsync(long trackerId, ulong userId);

    /// <summary>
    /// Stops a turn tracker.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <param name="userId">User id</param>
    /// <returns></returns>
    Task StopTrackerAsync(long trackerId, ulong userId);

    /// <summary>
    /// Updates a turn tracker.
    /// </summary>
    /// <param name="updatedTracker">Updated turn tracker parameters</param>
    /// <returns></returns>
    Task UpdateTrackerAsync(TurnTracker updatedTracker);

    /// <summary>
    /// Updates a turn in tracker.
    /// </summary>
    /// <param name="updatedTurn">Updated turn parameters</param>
    /// <returns></returns>
    Task UpdateTurnAsync(Turn updatedTurn);

    /// <summary>
    /// Removes a collection of turns.
    /// </summary>
    /// <param name="userId">User id</param>
    /// <param name="turnsIds">A collection of turn ids</param>
    /// <returns>Deleted turns count.</returns>
    Task<int> BulkDeleteTurnsAsync(ulong userId, IEnumerable<long> turnsIds);

    /// <summary>
    /// Deletes a turn tracker.
    /// </summary>
    /// <param name="trackerId">Tracker id</param>
    /// <param name="userId">User id</param>
    Task DeleteTrackerAsync(long trackerId, ulong userId);

    /// <summary>
    /// Deletes old private turn trackers.
    /// </summary>
    /// <param name="interval">Delete interval</param>
    /// <returns>Deletes turn trackers count.</returns>
    Task<int> CleanPrivateTrackersByIntervalAsync(TimeSpan interval);
}