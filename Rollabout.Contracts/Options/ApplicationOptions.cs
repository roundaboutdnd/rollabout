namespace Rollabout.Contracts.Options;

/// <summary>
/// Base object for application options.
/// </summary>
public abstract class ApplicationOptions
{
    /// <summary>
    /// ConfigurationSection name.
    /// </summary>
    public static string ConfigurationSectionName => "Application";
}