namespace Rollabout.Contracts.Entities;

/// <summary>
/// Coin flip object.
/// </summary>
public sealed record CoinFlips
{
    /// <summary>
    /// Coins count.
    /// </summary>
    public int CoinsCount { get; init; }

    /// <summary>
    /// "Heads" count.
    /// </summary>
    public int HeadsCount { get; init; }

    /// <summary>
    /// "Tails" count.
    /// </summary>
    public int TailsCount { get; init; }
}