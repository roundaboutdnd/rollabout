namespace Rollabout.Contracts.Entities;

/// <summary>
/// Math actions.
/// </summary>
public enum MathActions
{
    /// <summary>
    /// Addition operation.
    /// </summary>
    Add = '+',

    /// <summary>
    /// Subtract operation.
    /// </summary>
    Subtract = '-',

    /// <summary>
    /// Multiplication operation.
    /// </summary>
    Multiply = '*',

    /// <summary>
    /// Division operation.
    /// </summary>
    Divide = '/',

    /// <summary>
    /// Power operation.
    /// </summary>
    Power = '^',

    /// <summary>
    /// Modulus operation.
    /// </summary>
    Modulus = '%'
}