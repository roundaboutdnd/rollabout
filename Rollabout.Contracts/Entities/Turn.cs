namespace Rollabout.Contracts.Entities;

/// <summary>
/// Turn object from a turn tracker.
/// </summary>
public sealed record Turn
{
    /// <summary>
    /// Turn id.
    /// </summary>
    public long Id { get; init; }

    /// <summary>
    /// Turn tracker id.
    /// </summary>
    public long TrackerId { get; init; }

    /// <summary>
    /// User id.
    /// </summary>
    public ulong UserId { get; init; }

    /// <summary>
    /// Turn name.
    /// </summary>
    public string? Name { get; init; }

    /// <summary>
    /// Evaluated expression value.
    /// </summary>
    public double? Value { get; init; }
}