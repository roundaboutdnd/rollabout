using System.Collections.Generic;

namespace Rollabout.Contracts.Entities;

/// <summary>
/// API error object.
/// </summary>
public sealed record ApiError
{
    /// <summary>
    /// Error type.
    /// </summary>
    public string? Type { get; set; }

    /// <summary>
    /// Error message title.
    /// </summary>
    public string? Title { get; set; } = default;

    /// <summary>
    /// HTTP status code.
    /// </summary>
    public int? Status { get; set; }

    /// <summary>
    /// Error message details.
    /// </summary>
    public string? Detail { get; set; } = default;

    /// <summary>
    /// Class instance where exception was thrown.
    /// </summary>
    public string? Instance { get; set; }

    /// <summary>
    /// Error id.
    /// </summary>
    public string? TraceId { get; set; } = default;

    /// <summary>
    /// A collection of aggregated error messages.
    /// </summary>
    public Dictionary<string, IEnumerable<string>>? Errors { get; set; } = default;
}