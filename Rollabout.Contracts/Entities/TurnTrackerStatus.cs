namespace Rollabout.Contracts.Entities;

/// <summary>
/// Turn tracker status information.
/// </summary>
public sealed record TurnTrackerStatus
{
    /// <summary>
    /// Current turn information.
    /// </summary>
    public Turn? CurrentTurn { get; init; }

    /// <summary>
    /// Next turn information.
    /// </summary>
    public Turn? NextTurn { get; init; }
}