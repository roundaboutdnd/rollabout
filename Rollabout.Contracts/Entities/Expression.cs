namespace Rollabout.Contracts.Entities;

/// <summary>
/// Engine expression object.
/// </summary>
public sealed record Expression
{
    /// <summary>
    /// Expression string.
    /// </summary>
    public string OriginalString { get; init; } = null!;

    /// <summary>
    /// Whether to use ANSI formatting (rich text output).
    /// </summary>
    public bool UseAnsiFormatting { get; init; }

    /// <summary>
    /// Evaluation result string.
    /// </summary>
    public string ResultString { get; init; } = null!;

    /// <summary>
    /// Expression result value.
    /// </summary>
    public double ResultValue { get; init; }

    /// <summary>
    /// Whether result satisfies Target Number condition.
    /// </summary>
    public bool IsTargetNumberResult { get; init; }

    /// <summary>
    /// Whether result satisfies Match condition.
    /// </summary>
    public bool IsMatchResult { get; init; }

    /// <summary>
    /// Additional text for messages.
    /// </summary>
    public string? ResultAdditionalText { get; init; }
}