namespace Rollabout.Contracts.Entities;

/// <summary>
/// Turn tracker object information.
/// </summary>
public sealed record TurnTracker
{
    /// <summary>
    /// Turn tracker id.
    /// </summary>
    public long Id { get; init; }

    /// <summary>
    /// Chat id.
    /// </summary>
    public ulong ChatId { get; init; }

    /// <summary>
    /// User id.
    /// </summary>
    public ulong UserId { get; init; }

    /// <summary>
    /// Message id.
    /// </summary>
    public ulong? MessageId { get; init; }

    /// <summary>
    /// Current turn information.
    /// </summary>
    public long? CurrentTurnId { get; init; }

    /// <summary>
    /// Next turn information.
    /// </summary>
    public long? NextTurnId { get; init; }

    /// <summary>
    /// Whether this tracker is private or not.
    /// </summary>
    public bool Private { get; init; }
}