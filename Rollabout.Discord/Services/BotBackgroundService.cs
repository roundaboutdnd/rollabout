using System.Diagnostics.CodeAnalysis;
using System.Net;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.EventArgs;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Rollabout.Contracts.Exceptions;

namespace Rollabout.Discord.Services;

internal class BotBackgroundService : BackgroundService
{
    private readonly IOptionsMonitor<DiscordApplicationOptions> _applicationOptions;
    private readonly DiscordShardedClient _discordShardedClient;

    public BotBackgroundService(IHostApplicationLifetime hostApplicationLifetime,
        IOptionsMonitor<DiscordApplicationOptions> applicationOptions, DiscordShardedClient discordShardedClient,
        InteractivityConfiguration interactivityConfiguration)
    {
        _applicationOptions = applicationOptions;
        _discordShardedClient = discordShardedClient;
        _discordShardedClient.UnknownEvent += OnUnknownEvent;

        hostApplicationLifetime.ApplicationStarted.Register(() =>
        {
            _discordShardedClient.UseInteractivityAsync(interactivityConfiguration);

            foreach (var shardClient in _discordShardedClient.ShardClients.Values)
            {
                var slashCommands = shardClient.GetSlashCommands();

                slashCommands.SlashCommandErrored += OnSlashCommandErrored;
            }

            // we need shards to be initialized before doing anything with them
            _discordShardedClient.StartAsync().Wait();
        });

        hostApplicationLifetime.ApplicationStopped.Register(() =>
        {
            if (hostApplicationLifetime.ApplicationStarted.IsCancellationRequested)
            {
                // we need shards to be stopped before application exits
                _discordShardedClient.StopAsync().Wait();
            }
        });
    }

    // this is just to healthcheck our API periodically, DiscordShardedClient does all real work for us
    [SuppressMessage("ReSharper", "TemplateIsNotCompileTimeConstantProblem")]
    [SuppressMessage("Usage", "CA2254:Template should be a static expression")]
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        if (bool.TryParse(Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER"), out var runningInDocker) &&
            runningInDocker)
            return;

        using var httpClient = new HttpClient();
        while (!stoppingToken.IsCancellationRequested)
        {
            var logLevel = LogLevel.Debug;
            var statusCode = HttpStatusCode.OK;
            var message = $"Ping API in GET:/healthcheck: \"GET:/healthcheck\" -> \"{(int)statusCode}\"";
            try
            {
                var response = await httpClient.GetAsync(
                    $"{_applicationOptions.CurrentValue.ApiEndpoint.Replace("v1", string.Empty).Trim('/')}/healthcheck",
                    stoppingToken);

                statusCode = response.StatusCode;
            }
            catch (HttpRequestException ex)
            {
                statusCode = ex.StatusCode ?? HttpStatusCode.InternalServerError;
                logLevel = LogLevel.Error;
                message = "API is not responding correctly";
            }
            finally
            {
                _discordShardedClient.Logger.Log(logLevel, new EventId((int)statusCode, "Healthcheck"), message);
            }

            await Task.Delay(_applicationOptions.CurrentValue.PingInterval, stoppingToken);
        }
    }

    private async Task OnUnknownEvent(DiscordClient sender, UnknownEventArgs e)
    {
        switch (e.EventName)
        {
            case "APPLICATION_COMMAND_PERMISSIONS_UPDATE":
                var eventArgs = JsonConvert.DeserializeObject<DiscordGuildApplicationCommandPermissions>(e.Json);
                if (eventArgs == null)
                    break;
                var guildId = eventArgs.GuildId;
                var guildCommands = sender.GetSlashCommands().RegisteredCommands
                    .FirstOrDefault(c => c.Key == guildId).Value;
                if (guildCommands == null)
                    break;
                await sender.BulkOverwriteGuildApplicationCommandsAsync(guildId, guildCommands);
                break;
            case "APPLICATION_UPDATE":
            {
                foreach (var client in _discordShardedClient.ShardClients.Values)
                {
                    var globalCommands = client.GetSlashCommands().RegisteredCommands.FirstOrDefault(c => c.Key == null)
                        .Value;
                    if (globalCommands == null)
                        break;
                    await client.BulkOverwriteGlobalApplicationCommandsAsync(globalCommands);
                }

                break;
            }
        }
    }

    private static async Task OnSlashCommandErrored(SlashCommandsExtension sender, SlashCommandErrorEventArgs e)
    {
        var exception = e.Exception;
        if (exception is not RollaboutApiException rollaboutException)
            throw exception;

        var error = rollaboutException.Error;
        var errorMessageBuilder = new DiscordEmbedBuilder();

        errorMessageBuilder.WithColor(DiscordColor.Red);
        errorMessageBuilder.WithTitle(error.Title);
        errorMessageBuilder.WithDescription(error.Errors?.Count > 0
            ? string.Join(Environment.NewLine, error.Errors.SelectMany(x => x.Value))
            : error.Detail);
        errorMessageBuilder.WithFooter(error.TraceId);

        await e.Context.CreateResponseAsync(errorMessageBuilder.Build(), true);
        throw e.Exception;
    }
}