using Microsoft.Extensions.Hosting;

namespace Rollabout.Discord.Services;

internal interface IBotStatsBackgroundService : IHostedService
{
}