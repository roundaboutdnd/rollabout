using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using DSharpPlus;
using DSharpPlus.EventArgs;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Rollabout.Discord.Services;

internal sealed class BotStatsBackgroundService : BackgroundService, IBotStatsBackgroundService
{
    private readonly IOptionsMonitor<DiscordApplicationOptions> _applicationOptions;
    private readonly DiscordShardedClient _discordShardedClient;
    private readonly HttpClient _httpClient;

    public BotStatsBackgroundService(IOptionsMonitor<DiscordApplicationOptions> applicationOptions,
        DiscordShardedClient discordShardedClient)
    {
        _applicationOptions = applicationOptions;
        _discordShardedClient = discordShardedClient;
        _httpClient = new HttpClient();

        _discordShardedClient.GuildCreated += UpdateBotStatsAsync;
        _discordShardedClient.GuildDeleted += UpdateBotStatsAsync;
    }

    public override void Dispose()
    {
        _httpClient.Dispose();
        base.Dispose();
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken) => Task.CompletedTask;

    private async Task UpdateBotStatsAsync(DiscordClient sender, DiscordEventArgs e)
    {
        var guildsCount = _discordShardedClient.ShardClients.Values.SelectMany(shard => shard.Guilds.Values).Count();
        var content = new Dictionary<string, object>
        {
            { "server_count", guildsCount },
            { "bot_id", _discordShardedClient.CurrentUser.Id.ToString() }
        };

        foreach (var (api, token) in _applicationOptions.CurrentValue.StatsApiTokens!)
        {
            if (string.IsNullOrEmpty(token))
                continue;

            content.TryAdd(api, token);
        }

        try
        {
            var response = await _httpClient.PostAsJsonAsync("https://botblock.org/api/count", content);
            if (!response.IsSuccessStatusCode)
            {
                var statusCode = (int)response.StatusCode;

                _discordShardedClient.Logger.Log(LogLevel.Error, new EventId(statusCode, "Stats"), "{Message}",
                    $"""Updating stats in POST:/bots/:bot_id/stats: "POST:/bots/:bot_id/stats" -> "{statusCode}" """);

                return;
            }

            var responseContent = await response.Content.ReadFromJsonAsync<BotBlockResponse>();
            if (responseContent == null)
            {
                _discordShardedClient.Logger.Log(LogLevel.Warning, new EventId((int)response.StatusCode, "Stats"),
                    "{Message}",
                    """Updating stats in POST:/bots/:bot_id/stats: "POST:/bots/:bot_id/stats" -> "null" """);

                return;
            }

            foreach (var apiResponse in responseContent.Success ?? new Dictionary<string, object[]>())
            {
                _discordShardedClient.Logger.Log(LogLevel.Information,
                    new EventId(((JsonElement)apiResponse.Value[0]).GetInt32(), "Stats"), "{Message}",
                    $"""Updating stats in POST:/bots/:bot_id/stats: "POST:/bots/:bot_id/stats" -> "{apiResponse.Key}" """);
            }

            foreach (var apiResponse in responseContent.Failure ?? new Dictionary<string, object[]>())
            {
                _discordShardedClient.Logger.Log(LogLevel.Warning,
                    new EventId(((JsonElement)apiResponse.Value[0]).GetInt32(), "Stats"), "{Message}",
                    $"""Updating stats in POST:/bots/:bot_id/stats: "POST:/bots/:bot_id/stats" -> "{apiResponse.Key}" """);
            }
        }
        catch (Exception ex)
        {
            _discordShardedClient.Logger.Log(LogLevel.Error, ex, "{Message}", ex.Message);
        }
    }
}

[Serializable]
internal sealed record BotBlockResponse
{
    [JsonPropertyName("success")] public Dictionary<string, object[]>? Success { get; set; }

    [JsonPropertyName("failure")] public Dictionary<string, object[]>? Failure { get; set; }
}