using Microsoft.Extensions.Hosting;

namespace Rollabout.Discord.Services;

[Obsolete("This is a stub class for dependency injection purposes only.")]
internal sealed class BotStatsStubBackgroundService : BackgroundService, IBotStatsBackgroundService
{
    protected override Task ExecuteAsync(CancellationToken stoppingToken) => Task.CompletedTask;
}