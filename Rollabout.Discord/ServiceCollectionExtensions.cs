using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Rollabout.Discord;

internal static class ServiceCollectionExtensions
{
    public static IServiceCollection AddConfiguration<T>(this IServiceCollection serviceCollection,
        IConfiguration configuration) where T : class
    {
        var configurationSection = configuration.GetSection(typeof(T).Name);
        var instance = Activator.CreateInstance<T>();

        configurationSection.Bind(instance, o => o.BindNonPublicProperties = true);
        serviceCollection.AddSingleton(instance);

        return serviceCollection;
    }
}