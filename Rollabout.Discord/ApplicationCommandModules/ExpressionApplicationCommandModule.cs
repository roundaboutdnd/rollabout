using System.Text;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using Microsoft.Extensions.Options;
using Rollabout.Contracts.Entities;

namespace Rollabout.Discord.ApplicationCommandModules;

internal class ExpressionApplicationCommandModule : ApplicationCommandModuleBase
{
    private const string RollButtonId = "rollabout_roll:";
    private const string StopButtonId = "rollabout_stop";

    private readonly DiscordButtonComponent _stopButton = new(ButtonStyle.Secondary, StopButtonId, "Keep it");

    public ExpressionApplicationCommandModule(IOptionsMonitor<DiscordApplicationOptions> applicationOptions)
        : base(applicationOptions, DiscordColor.Black)
    {
    }

    [SlashCommand("roll", "Evaluates a dice roll/math expression and returns it's result")]
    public async Task RollCommandAsync(InteractionContext ctx,
        [Option("expression",
            "A dice roll/math expression. If no parameter specified, this command will roll 1d20 automatically")]
        string? expression = default)
    {
        var interactionResponseBuilder = await EvaluateExpressionAsync(expression);
        if (interactionResponseBuilder == null)
            return;

        await ctx.CreateResponseAsync(interactionResponseBuilder);

        var interactivityExtension = ctx.Client.GetInteractivity();
        var originalMessage = await ctx.GetOriginalResponseAsync();
        var interactivityResult = await interactivityExtension.WaitForButtonAsync(originalMessage);
        var cts = new CancellationTokenSource();
        while (!cts.IsCancellationRequested && !interactivityResult.TimedOut)
        {
            var componentInteractionCreateEventArgs = interactivityResult.Result;
            if (componentInteractionCreateEventArgs.Id.Contains(RollButtonId))
            {
                await ProcessRollAgainButtonAsync(componentInteractionCreateEventArgs, originalMessage, cts.Token);

                originalMessage = await ctx.GetOriginalResponseAsync();
                interactivityResult = await interactivityExtension.WaitForButtonAsync(originalMessage);
            }
            else if (componentInteractionCreateEventArgs.Id == StopButtonId)
            {
                cts.Cancel();
            }
        }

        var messageBuilder = new DiscordMessageBuilder();

        messageBuilder.AddEmbeds(originalMessage.Embeds);

        await originalMessage.ModifyAsync(messageBuilder);
    }

    [SlashCommand("flip", "Flips a given number of coins and returns heads/tails count")]
    public async Task CoinFlipCommandAsync(InteractionContext ctx,
        [Option("coins",
            "Coin flips count. If no parameter specified, this command will flip 1 coin automatically")]
        double coins = 1)
    {
        var apiResponse = await ReceiveFromApiAsync<CoinFlips>(HttpMethod.Get,
            $"/Expression/FlipCoins?coinsCount={coins}");

        if (apiResponse == null)
            return;

        var embedBuilder = new DiscordEmbedBuilder();

        embedBuilder.WithColor(MessageColor);
        embedBuilder.WithTitle(
            $"Flipping {apiResponse.CoinsCount} coin{(apiResponse.CoinsCount > 1 ? "s" : string.Empty)}:");

        embedBuilder.WithDescription(string.Join(Environment.NewLine, $"{apiResponse.HeadsCount} heads",
            $"{apiResponse.TailsCount} tails"));

        await ctx.CreateResponseAsync(embedBuilder.Build());
    }

    private async Task ProcessRollAgainButtonAsync(ComponentInteractionCreateEventArgs e,
        DiscordMessage originalMessage, CancellationToken cancellationToken)
    {
        var discordInteraction = e.Interaction;
        await discordInteraction.CreateResponseAsync(InteractionResponseType.DeferredMessageUpdate);
        var title = string.Empty;
        var descriptionBuilder = new StringBuilder();
        foreach (var embed in originalMessage.Embeds)
        {
            if (string.IsNullOrEmpty(title))
            {
                title = embed.Title;
            }

            descriptionBuilder.AppendLine(embed.Description.TrimEnd('`'));
        }

        var interactionResponseBuilder = await EvaluateExpressionAsync(e.Id.Replace(RollButtonId, string.Empty),
            false, cancellationToken);

        if (cancellationToken.IsCancellationRequested || interactionResponseBuilder == null)
            return;

        foreach (var embed in interactionResponseBuilder.Embeds)
        {
            descriptionBuilder.Append(embed.Description);
        }

        descriptionBuilder.Append("```");

        var embedBuilder = new DiscordEmbedBuilder();

        embedBuilder.WithColor(MessageColor);
        embedBuilder.WithTitle(title);
        embedBuilder.WithDescription(descriptionBuilder.ToString());

        var webhookBuilder = new DiscordWebhookBuilder();

        webhookBuilder.WithContent(interactionResponseBuilder.Content);
        webhookBuilder.AddEmbeds(new[] { embedBuilder.Build() });
        webhookBuilder.AddComponents(interactionResponseBuilder.Components);

        await discordInteraction.EditOriginalResponseAsync(webhookBuilder);
    }

    private async Task<DiscordInteractionResponseBuilder?> EvaluateExpressionAsync(string? expression = default,
        bool useAnsi = true, CancellationToken cancellationToken = default)
    {
        var innerExpression = expression;
        if (string.IsNullOrEmpty(innerExpression) || string.IsNullOrWhiteSpace(innerExpression))
        {
            innerExpression = "1d20";
        }

        var apiResponse = await ReceiveFromApiAsync<Expression>(HttpMethod.Post, "/Expression/Evaluate",
            new
            {
                Expression = innerExpression,
                UseAnsiFormatting = true
            }, cancellationToken: cancellationToken);

        if (cancellationToken.IsCancellationRequested || apiResponse == null)
            return default;

        var embedBuilder = new DiscordEmbedBuilder();

        embedBuilder.WithColor(MessageColor);
        embedBuilder.WithTitle((apiResponse.ResultAdditionalText ?? $"Rolling {apiResponse.OriginalString}") + ":");

        var descriptionBuilder = new StringBuilder();

        descriptionBuilder.Append(
            apiResponse.ResultString.Contains(((MathActions[])Enum.GetValues(typeof(MathActions))).Select(c => (char)c)
                .ToArray())
                ? string.Join('=', apiResponse.ResultString, apiResponse.ResultValue)
                : apiResponse.ResultString);

        if (apiResponse.IsTargetNumberResult)
        {
            descriptionBuilder.Append($" Success{(apiResponse.ResultValue > 1 ? "es" : string.Empty)}");
        }
        else if (apiResponse.IsMatchResult)
        {
            descriptionBuilder.Append($" Match{(apiResponse.ResultValue > 1 ? "es" : string.Empty)}");
        }

        embedBuilder.WithDescription(useAnsi
            ? string.Join(descriptionBuilder.ToString(), $"```ansi{Environment.NewLine}", "```")
            : descriptionBuilder.ToString());

        var interactionResponseBuilder = new DiscordInteractionResponseBuilder();

        interactionResponseBuilder.WithContent("All results will save after 1 minute of inactivity.");
        interactionResponseBuilder.AddEmbed(embedBuilder.Build());
        interactionResponseBuilder.AddComponents(
            new DiscordButtonComponent(ButtonStyle.Primary, $"{RollButtonId}{innerExpression}", "Roll again!"),
            _stopButton
        );

        return interactionResponseBuilder;
    }
}