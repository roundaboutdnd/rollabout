using System.Diagnostics;
using System.Text;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Exceptions;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using Microsoft.Extensions.Options;
using Rollabout.Contracts.Entities;
using Rollabout.Contracts.Exceptions;

namespace Rollabout.Discord.ApplicationCommandModules;

[SlashCommandGroup("tracker", "Turn/initiative tracker")]
internal class TurnTrackerApplicationCommandModule : ApplicationCommandModuleBase
{
    private const string ConfirmLabel = "Confirm";
    private const string ConfirmEmoji = "👍";
    private const string AddTurnLabel = "Add Turn";
    private const string EditTurnLabel = "Edit Turn";
    private const string TurnNameLabel = "Turn name";
    private const string ExpressionLabel = "Dice roll/math expression";
    private const string NameFieldPlaceholder = "Each turn must have a unique name";
    private const string ExpressionFieldPlaceholder = "ex: 1d20+5, or 15";
    private const string ConfirmButtonId = "rollabout_button_confirm";
    private const string CancelButtonId = "rollabout_button_cancel";
    private const string AddButtonId = "rollabout_tracker_button_add";
    private const string EditButtonId = "rollabout_tracker_button_edit";
    private const string NextButtonId = "rollabout_tracker_button_next";
    private const string RemoveButtonId = "rollabout_tracker_button_remove";
    private const string StartButtonId = "rollabout_tracker_button_start";
    private const string StopButtonId = "rollabout_tracker_button_stop";
    private const string AddModalId = "rollabout_tracker_modal_add";
    private const string EditModalId = "rollabout_tracker_modal_edit";
    private const string ModalNameFieldId = "rollabout_tracker_modal_field_name";
    private const string ModalExpressionFieldId = "rollabout_tracker_modal_field_expression";
    private const string SelectTurnId = "rollabout_tracker_select_turn";
    private const string BulkSelectTurnsId = "rollabout_tracker_select_turns";

    private readonly DiscordInteractionResponseBuilder _addModalBuilder = new();
    private readonly DiscordInteractionResponseBuilder _editModalBuilder = new();

    private readonly IEnumerable<DiscordButtonComponent> _confirmationButtons = new[]
    {
        new DiscordButtonComponent(ButtonStyle.Success, ConfirmButtonId, ConfirmLabel,
            emoji: new DiscordComponentEmoji(DiscordEmoji.FromUnicode(ConfirmEmoji))),
        new DiscordButtonComponent(ButtonStyle.Danger, CancelButtonId, "Cancel",
            emoji: new DiscordComponentEmoji(DiscordEmoji.FromUnicode("🚫")))
    };

    public TurnTrackerApplicationCommandModule(IOptionsMonitor<DiscordApplicationOptions> applicationOptions)
        : base(applicationOptions, DiscordColor.Orange)
    {
        _addModalBuilder.WithTitle(AddTurnLabel);
        _addModalBuilder.WithCustomId(AddModalId);

        // modal fields must be added as separate ActionRows
        _addModalBuilder.AddComponents(new TextInputComponent(TurnNameLabel, ModalNameFieldId, NameFieldPlaceholder));
        _addModalBuilder.AddComponents(new TextInputComponent(ExpressionLabel, ModalExpressionFieldId,
            ExpressionFieldPlaceholder));

        _editModalBuilder.WithTitle(EditTurnLabel);
        _editModalBuilder.WithCustomId(EditModalId);
        _editModalBuilder.AddComponents(new TextInputComponent(TurnNameLabel, ModalNameFieldId, NameFieldPlaceholder,
            required: false));

        _editModalBuilder.AddComponents(new TextInputComponent(ExpressionLabel, ModalExpressionFieldId,
            ExpressionFieldPlaceholder, required: false));
    }

    [SlashCommand("create", "Creates a new turn tracker")]
    public async Task CreateTrackerCommandAsync(InteractionContext ctx,
        [Option("private", "Whether to hide created tracker from other users on this channel")]
        bool isPrivate = false,
        [Option("force", "Whether to create a new turn tracker without confirmation if it already exists")]
        bool force = false)
    {
        DiscordInteraction? discordInteraction = default;
        var tracker = await GetTrackerAsync(ctx.Channel.Id, ctx.User.Id);
        if (tracker != null && !isPrivate)
        {
            if (!force)
            {
                var interactionResponseBuilder = new DiscordInteractionResponseBuilder();

                interactionResponseBuilder.WithContent(string.Join(Environment.NewLine,
                    "You've already created an unhidden turn tracker. You can make it visible to chat by typing `/tracker get` command.",
                    $"If you still want to create a new tracker, you have to confirm this action by pressing ` {ConfirmEmoji} {ConfirmLabel} ` button below.",
                    "Old tracker and its turns will be deleted."));

                interactionResponseBuilder.AddComponents(_confirmationButtons);
                interactionResponseBuilder.AsEphemeral();

                await ctx.CreateResponseAsync(interactionResponseBuilder);
                var interactivityExtension = ctx.Client.GetInteractivity();
                var interactivityResult = await interactivityExtension.WaitForButtonAsync(
                    await ctx.GetOriginalResponseAsync());

                if (interactivityResult.TimedOut)
                {
                    await ctx.DeleteResponseAsync();
                    return;
                }

                discordInteraction = interactivityResult.Result.Interaction;

                await discordInteraction.CreateResponseAsync(InteractionResponseType.DeferredMessageUpdate);
                switch (interactivityResult.Result.Id)
                {
                    case ConfirmButtonId:
                        await DeleteTrackerAsync(tracker.Id, ctx.User.Id);
                        await discordInteraction.DeleteOriginalResponseAsync();
                        break;
                    case CancelButtonId:
                        await ctx.DeleteResponseAsync();
                        return;
                }
            }
            else
            {
                await DeleteTrackerAsync(tracker.Id, ctx.User.Id);
            }
        }

        tracker = await ReceiveFromApiAsync<TurnTracker>(HttpMethod.Post, "/TurnTracker",
            new
            {
                ChatId = ctx.Channel.Id,
                UserId = ctx.User.Id,
                Private = isPrivate
            }, true);

        await ProcessTurnTracker(ctx, tracker!, discordInteraction);
    }

    [SlashCommand("get", "Gets previously created turn tracker")]
    public async Task GetTrackerCommandAsync(InteractionContext ctx)
    {
        var tracker = await GetTrackerAsync(ctx.Channel.Id, ctx.User.Id);
        if (tracker == null)
        {
            await ctx.CreateResponseAsync(string.Join(Environment.NewLine,
                "You haven't created any turn tracker on this channel yet!",
                "If you want to create one, use `/tracker create` command instead."), true);
        }
        else
        {
            await ProcessTurnTracker(ctx, tracker);
        }
    }

    [SlashCommand("delete", "Deletes previously created turn tracker")]
    public async Task DeleteTrackerCommandAsync(InteractionContext ctx,
        [Option("force", "Whether to delete a turn tracker without confirmation")]
        bool force = false)
    {
        var tracker = await GetTrackerAsync(ctx.Channel.Id, ctx.User.Id);
        if (tracker == null)
        {
            await ctx.CreateResponseAsync("You haven't created any turn tracker on this channel yet!", true);
        }
        else
        {
            if (!force)
            {
                var interactionResponseBuilder = new DiscordInteractionResponseBuilder();

                interactionResponseBuilder.WithContent(
                    $"If you want to delete an existing turn tracker, you have to confirm this action by pressing ` {ConfirmEmoji} {ConfirmLabel} ` button below.");

                interactionResponseBuilder.AddComponents(_confirmationButtons);
                interactionResponseBuilder.AsEphemeral();

                await ctx.CreateResponseAsync(interactionResponseBuilder);

                var interactivityExtension = ctx.Client.GetInteractivity();
                var interactivityResult = await interactivityExtension.WaitForButtonAsync(
                    await ctx.GetOriginalResponseAsync());

                if (interactivityResult.TimedOut)
                {
                    await ctx.DeleteResponseAsync();
                    return;
                }

                var discordInteraction = interactivityResult.Result.Interaction;
                await discordInteraction.CreateResponseAsync(InteractionResponseType.DeferredMessageUpdate);
                switch (interactivityResult.Result.Id)
                {
                    case ConfirmButtonId:
                        await DeleteTrackerAsync(tracker.Id, ctx.User.Id);
                        await discordInteraction.EditOriginalResponseAsync(new DiscordWebhookBuilder().WithContent(
                            "Successfully deleted your turn tracker. You can dismiss this message."));
                        break;
                    case CancelButtonId:
                        await ctx.DeleteResponseAsync();
                        return;
                }
            }
            else
            {
                await DeleteTrackerAsync(tracker.Id, ctx.User.Id);
                await ctx.CreateResponseAsync(
                    "Successfully deleted your turn tracker. You can dismiss this message.", true);
            }
        }
    }

    private async Task ProcessTurnTracker(InteractionContext ctx, TurnTracker tracker,
        DiscordInteraction? discordInteraction = default)
    {
        var buttonRows = new DiscordActionRowComponent[]
        {
            new(new[]
            {
                new DiscordButtonComponent(ButtonStyle.Secondary, AddButtonId, AddTurnLabel,
                    emoji: new DiscordComponentEmoji(DiscordEmoji.FromUnicode("➕"))),
                new DiscordButtonComponent(ButtonStyle.Secondary, EditButtonId, EditTurnLabel, true,
                    new DiscordComponentEmoji(DiscordEmoji.FromUnicode("✏"))),
                new DiscordButtonComponent(ButtonStyle.Secondary, RemoveButtonId, "Remove Turns", true,
                    new DiscordComponentEmoji(DiscordEmoji.FromUnicode("🗑️")))
            }),
            new(new[]
            {
                new DiscordButtonComponent(ButtonStyle.Success, StartButtonId, "Start", true,
                    new DiscordComponentEmoji(DiscordEmoji.FromUnicode("▶"))),
                new DiscordButtonComponent(ButtonStyle.Primary, NextButtonId, "Next Turn", true,
                    new DiscordComponentEmoji(DiscordEmoji.FromUnicode("⏩"))),
                new DiscordButtonComponent(ButtonStyle.Danger, StopButtonId, "Stop", true,
                    new DiscordComponentEmoji(DiscordEmoji.FromUnicode("🛑")))
            })
        };

        DiscordMessage? originalMessage;

        var turns = await GetTurnsAsync(tracker);
        var trackerStatus = await GetTrackerStatusAsync(tracker.Id);
        if (discordInteraction == null)
        {
            await ctx.CreateResponseAsync(
                BuildTrackerResponse<DiscordInteractionResponseBuilder>(buttonRows, trackerStatus, turns,
                    tracker.Private));

            originalMessage = await ctx.GetOriginalResponseAsync();
        }
        else
        {
            originalMessage = await discordInteraction.CreateFollowupMessageAsync(
                BuildTrackerResponse<DiscordFollowupMessageBuilder>(buttonRows, trackerStatus, turns, tracker.Private));
        }

        await ExecuteApiAsync(HttpMethod.Put, $"/TurnTracker/{tracker.Id}", new
        {
            UserId = ctx.User.Id,
            MessageId = originalMessage.Id
        }, true);

        tracker = await GetTrackerAsync(tracker.Id);

        var interactivityExtension = ctx.Client.GetInteractivity();
        var stopwatch = Stopwatch.StartNew();
        var interactivityResult = await interactivityExtension.WaitForButtonAsync(originalMessage);
        var cts = new CancellationTokenSource();
        Task? task = default;
        while (stopwatch.Elapsed.Hours < 1)
        {
            if (!cts.TryReset())
            {
                cts = new CancellationTokenSource();
            }
            else if (task is
                     {
                         Status: not (TaskStatus.RanToCompletion or TaskStatus.Canceled or TaskStatus.Faulted)
                     })
            {
                cts.Cancel();
                continue;
            }

            var componentInteractionCreateEventArgs = interactivityResult.Result;
            if (componentInteractionCreateEventArgs != null)
            {
                var buttonId = componentInteractionCreateEventArgs.Id;

                task = (buttonId switch
                {
                    AddButtonId => AddTurnAsync(tracker, interactivityExtension,
                        componentInteractionCreateEventArgs, cts.Token),
                    EditButtonId => EditTurnAsync(tracker, interactivityExtension,
                        componentInteractionCreateEventArgs, cts.Token),
                    RemoveButtonId => RemoveTurnsAsync(tracker, interactivityExtension,
                        componentInteractionCreateEventArgs, cts.Token),
                    StartButtonId => StartAsync(tracker, componentInteractionCreateEventArgs, cts.Token),
                    NextButtonId => NextTurnAsync(tracker, componentInteractionCreateEventArgs, cts.Token),
                    StopButtonId => StopAsync(tracker, componentInteractionCreateEventArgs, cts.Token),
                    _ => Task.CompletedTask
                }).ContinueWith(t =>
                {
                    if (!t.IsFaulted)
                        return t;

                    var exception = t.Exception?.InnerException as RollaboutApiException;
                    var error = exception?.Error;
                    if (error == null)
                        return t;

                    var errorEmbedBuilder = new DiscordEmbedBuilder();

                    errorEmbedBuilder.WithColor(DiscordColor.Red);
                    errorEmbedBuilder.WithTitle(error.Title);
                    errorEmbedBuilder.WithDescription(error.Errors?.Count > 0
                        ? string.Join(Environment.NewLine, error.Errors.SelectMany(x => x.Value))
                        : error.Detail);

                    errorEmbedBuilder.WithFooter(error.TraceId);

                    var followupMessageBuilder = new DiscordFollowupMessageBuilder();

                    followupMessageBuilder.AddEmbed(errorEmbedBuilder.Build());
                    followupMessageBuilder.AsEphemeral();

                    return componentInteractionCreateEventArgs.Interaction
                        .CreateFollowupMessageAsync(followupMessageBuilder);
                }, cts.Token);

                if (!string.IsNullOrEmpty(buttonId))
                {
                    stopwatch.Restart();
                }

                discordInteraction ??= componentInteractionCreateEventArgs.Interaction;

                try
                {
                    originalMessage = await discordInteraction.GetOriginalResponseAsync();
                }
                catch (NotFoundException)
                {
                    originalMessage = await discordInteraction.GetFollowupMessageAsync(originalMessage.Id);
                }
            }

            interactivityResult = await interactivityExtension.WaitForButtonAsync(originalMessage);
        }

        stopwatch.Stop();

        tracker = await GetTrackerAsync(tracker.Id, CancellationToken.None);

        if (tracker is { Private: true })
        {
            await DeleteTrackerCommandAsync(ctx, true);
        }

        await (discordInteraction == null
            ? ctx.DeleteResponseAsync()
            : discordInteraction.DeleteOriginalResponseAsync());
    }

    private async Task AddTurnAsync(TurnTracker tracker, InteractivityExtension interactivityExtension,
        ComponentInteractionCreateEventArgs e, CancellationToken cancellationToken)
    {
        var discordInteraction = e.Interaction;
        await discordInteraction.CreateResponseAsync(InteractionResponseType.Modal, _addModalBuilder);
        var submitModalResult = (await interactivityExtension.WaitForModalAsync(AddModalId, cancellationToken)).Result;
        if (cancellationToken.IsCancellationRequested || submitModalResult == null)
            return;

        var turnName = submitModalResult.Values[ModalNameFieldId];
        var expression = submitModalResult.Values[ModalExpressionFieldId];
        await submitModalResult.Interaction.CreateResponseAsync(InteractionResponseType.UpdateMessage);
        var evaluatedExpression = await EvaluateAsync(expression, cancellationToken);
        if (cancellationToken.IsCancellationRequested || evaluatedExpression == null)
            return;

        await ExecuteApiAsync(HttpMethod.Post, $"/TurnTracker/{tracker.Id}/CreateTurn", new
            {
                UserId = e.User.Id,
                Name = turnName,
                Value = evaluatedExpression.ResultValue
            },
            true, cancellationToken);

        if (cancellationToken.IsCancellationRequested)
            return;

        var turns = await GetTurnsAsync(tracker, cancellationToken: cancellationToken);
        var trackerStatus = await GetTrackerStatusAsync(tracker.Id, cancellationToken);
        if (!cancellationToken.IsCancellationRequested && turns != null)
        {
            await discordInteraction.EditOriginalResponseAsync(BuildTrackerResponse<DiscordWebhookBuilder>(e.Message,
                trackerStatus, turns, tracker.Private));
        }
    }

    private async Task EditTurnAsync(TurnTracker tracker, InteractivityExtension interactivityExtension,
        ComponentInteractionCreateEventArgs e, CancellationToken cancellationToken)
    {
        var discordInteraction = e.Interaction;
        await discordInteraction.CreateResponseAsync(InteractionResponseType.DeferredMessageUpdate);
        var turns = await GetTurnsAsync(tracker, e.User.Id, cancellationToken);
        if (cancellationToken.IsCancellationRequested || turns == null)
            return;

        var selectTurns = turns.Select(t => new DiscordSelectComponentOption(t.Name, t.Id.ToString())).ToList();
        if (selectTurns.Count == 0)
            return;

        var followupMessageBuilder = new DiscordFollowupMessageBuilder();

        followupMessageBuilder.WithContent(string.Join(Environment.NewLine,
            "Please select turns you want to edit on this tracker.",
            "You can edit only your own turns, unless you've created this tracker with `/tracker create` command."));

        followupMessageBuilder.AddComponents(new DiscordSelectComponent(SelectTurnId,
            "You can edit only one turn at a time.", selectTurns));

        followupMessageBuilder.AsEphemeral();

        var selectMessage = await discordInteraction.CreateFollowupMessageAsync(followupMessageBuilder);
        var submitSelectResult =
            (await interactivityExtension.WaitForSelectAsync(selectMessage, SelectTurnId, cancellationToken))
            .Result;

        if (cancellationToken.IsCancellationRequested || submitSelectResult == null)
            return;

        await submitSelectResult.Interaction.CreateResponseAsync(InteractionResponseType.Modal, _editModalBuilder);
        await submitSelectResult.Interaction.DeleteFollowupMessageAsync(selectMessage.Id);
        var submitModalResult = (await interactivityExtension.WaitForModalAsync(EditModalId, cancellationToken)).Result;
        if (cancellationToken.IsCancellationRequested || submitModalResult == null)
            return;

        await submitModalResult.Interaction.CreateResponseAsync(InteractionResponseType.DeferredMessageUpdate);
        Expression? evaluatedExpression = default;
        if (!string.IsNullOrEmpty(submitModalResult.Values[ModalExpressionFieldId]))
        {
            evaluatedExpression = await EvaluateAsync(submitModalResult.Values[ModalExpressionFieldId],
                cancellationToken);

            if (cancellationToken.IsCancellationRequested || evaluatedExpression == null)
                return;
        }

        await ExecuteApiAsync(HttpMethod.Put, $"/TurnTracker/Turns/{long.Parse(submitSelectResult.Values.First())}", new
        {
            TrackerId = tracker.Id,
            UserId = e.User.Id,
            NewName = string.IsNullOrEmpty(submitModalResult.Values[ModalNameFieldId])
                ? null
                : submitModalResult.Values[ModalNameFieldId],
            NewValue = evaluatedExpression?.ResultValue
        }, true, cancellationToken);

        if (cancellationToken.IsCancellationRequested)
            return;

        var updatedTurns = await GetTurnsAsync(tracker, cancellationToken: cancellationToken);
        var trackerStatus = await GetTrackerStatusAsync(tracker.Id, cancellationToken);
        if (!cancellationToken.IsCancellationRequested && updatedTurns != null)
        {
            await discordInteraction.EditOriginalResponseAsync(BuildTrackerResponse<DiscordWebhookBuilder>(e.Message,
                trackerStatus, updatedTurns, tracker.Private));
        }
    }

    private async Task RemoveTurnsAsync(TurnTracker tracker, InteractivityExtension interactivityExtension,
        ComponentInteractionCreateEventArgs e, CancellationToken cancellationToken)
    {
        var discordInteraction = e.Interaction;
        await discordInteraction.CreateResponseAsync(InteractionResponseType.DeferredMessageUpdate);
        var turns = await GetTurnsAsync(tracker, e.User.Id, cancellationToken);
        if (cancellationToken.IsCancellationRequested || turns == null)
            return;

        var selectTurns = turns.Select(t => new DiscordSelectComponentOption(t.Name, t.Id.ToString())).ToList();
        if (selectTurns.Count == 0)
            return;

        var followupMessageBuilder = new DiscordFollowupMessageBuilder();

        followupMessageBuilder.WithContent(string.Join(Environment.NewLine,
            "Please select turns you want to remove from this tracker.",
            "You can remove only your own turns, unless you've created this tracker with `/tracker create` command."));

        followupMessageBuilder.AddComponents(new DiscordSelectComponent(BulkSelectTurnsId,
            "You can remove multiple turns at a time.", selectTurns, maxOptions: selectTurns.Count));

        followupMessageBuilder.AsEphemeral();

        var selectMessage = await discordInteraction.CreateFollowupMessageAsync(followupMessageBuilder);
        var submitSelectResult =
            (await interactivityExtension.WaitForSelectAsync(selectMessage, BulkSelectTurnsId, cancellationToken))
            .Result;

        if (cancellationToken.IsCancellationRequested || submitSelectResult == null)
            return;

        await submitSelectResult.Interaction.CreateResponseAsync(InteractionResponseType.UpdateMessage);
        await submitSelectResult.Interaction.DeleteFollowupMessageAsync(selectMessage.Id);
        var turnIds = submitSelectResult.Values?.Select(long.Parse).ToArray() ?? new long[] { -1 };
        await ExecuteApiAsync(HttpMethod.Delete,
            $"/TurnTracker/Turns?userId={e.User.Id}&turnIds={string.Join("&turnIds=", turnIds)}", needsAuth: true,
            cancellationToken: cancellationToken);

        if (cancellationToken.IsCancellationRequested)
            return;

        var updatedTurns = await GetTurnsAsync(tracker, cancellationToken: cancellationToken);
        var trackerStatus = await GetTrackerStatusAsync(tracker.Id, cancellationToken);
        if (!cancellationToken.IsCancellationRequested)
        {
            await discordInteraction.EditOriginalResponseAsync(BuildTrackerResponse<DiscordWebhookBuilder>(e.Message,
                trackerStatus, updatedTurns, tracker.Private));
        }
    }

    private async Task StartAsync(TurnTracker tracker, ComponentInteractionCreateEventArgs e,
        CancellationToken cancellationToken)
    {
        var discordInteraction = e.Interaction;
        await discordInteraction.CreateResponseAsync(InteractionResponseType.UpdateMessage);
        var trackerStatus = await ReceiveFromApiAsync<TurnTrackerStatus>(HttpMethod.Post,
            $"/TurnTracker/{tracker.Id}/Start", new { UserId = e.User.Id }, true, cancellationToken);

        if (!cancellationToken.IsCancellationRequested && trackerStatus is { CurrentTurn: { }, NextTurn: { } })
        {
            await discordInteraction.EditOriginalResponseAsync(
                BuildTrackerResponse<DiscordWebhookBuilder>(e.Message, trackerStatus, isPrivate: tracker.Private));
        }
    }

    private async Task NextTurnAsync(TurnTracker tracker, ComponentInteractionCreateEventArgs e,
        CancellationToken cancellationToken)
    {
        var discordInteraction = e.Interaction;
        await discordInteraction.CreateResponseAsync(InteractionResponseType.UpdateMessage);
        var trackerStatus = await ReceiveFromApiAsync<TurnTrackerStatus>(HttpMethod.Post,
            $"/TurnTracker/{tracker.Id}/MoveToNextTurn", new { UserId = e.User.Id }, true, cancellationToken);

        if (!cancellationToken.IsCancellationRequested && trackerStatus is { CurrentTurn: { }, NextTurn: { } })
        {
            await discordInteraction.EditOriginalResponseAsync(
                BuildTrackerResponse<DiscordWebhookBuilder>(e.Message, trackerStatus, isPrivate: tracker.Private));
        }
    }

    private async Task StopAsync(TurnTracker tracker, ComponentInteractionCreateEventArgs e,
        CancellationToken cancellationToken)
    {
        var discordInteraction = e.Interaction;
        await discordInteraction.CreateResponseAsync(InteractionResponseType.UpdateMessage);
        await ExecuteApiAsync(HttpMethod.Post, $"/TurnTracker/{tracker.Id}/Stop", new { UserId = e.User.Id }, true,
            cancellationToken);

        if (!cancellationToken.IsCancellationRequested)
        {
            await discordInteraction.EditOriginalResponseAsync(
                BuildTrackerResponse<DiscordWebhookBuilder>(e.Message, isPrivate: tracker.Private));
        }
    }

    private T? BuildTrackerResponse<T>(DiscordMessage message, TurnTrackerStatus? trackerStatus = default,
        IEnumerable<Turn>? turns = default, bool isPrivate = false)
        where T : class
    {
        string? description = default;
        var descriptionBuilder = new StringBuilder();
        var nameBuilder = new StringBuilder();
        var valueBuilder = new StringBuilder();
        var turnsArray = turns?.ToArray();
        var turnsCount = turnsArray?.Length;
        var startedTracker = false;
        if (trackerStatus != null)
        {
            if (trackerStatus.CurrentTurn != null)
            {
                startedTracker = true;
                descriptionBuilder.AppendLine(
                    $"Current turn: **{trackerStatus.CurrentTurn.Name}** (<@{trackerStatus.CurrentTurn.UserId}>)");
            }

            if (trackerStatus.NextTurn != null)
            {
                startedTracker = true;
                descriptionBuilder.AppendLine(
                    $"Next turn: **{trackerStatus.NextTurn.Name}** (<@{trackerStatus.NextTurn.UserId}>)");
            }
        }

        if (turnsArray == null)
        {
            turnsCount = message.Embeds[0].Fields.Count;

            if (turnsCount > 0)
            {
                nameBuilder.AppendJoin(string.Empty,
                    message.Embeds[0].Fields.Where(f => f.Name == "Name").Select(f => f.Value));
                valueBuilder.AppendJoin(string.Empty,
                    message.Embeds[0].Fields.Where(f => f.Name == "Value").Select(f => f.Value));
            }
            else
            {
                description = "There aren't currently any entries on this tracker that take a turn!";
            }
        }
        else
        {
            if (turnsCount > 0)
            {
                foreach (var entry in turnsArray.OrderByDescending(t => t.Value))
                {
                    nameBuilder.AppendLine(entry.Name);
                    valueBuilder.AppendLine(entry.Value.ToString());
                }
            }
            else
            {
                description = "There aren't currently any entries on this tracker that take a turn!";
            }
        }

        var newComponentRows = message.Components.ToList();
        foreach (var component in newComponentRows.SelectMany(componentRow => componentRow.Components))
        {
            if (component is not DiscordButtonComponent button)
                continue;

            var _ = turnsCount switch
            {
                1 when button.CustomId is StartButtonId or NextButtonId or StopButtonId => button.Disable(),
                0 or null when button.CustomId is EditButtonId or RemoveButtonId or StartButtonId or NextButtonId
                    or StopButtonId => button.Disable(),
                _ => button.Enable()
            };

            switch (startedTracker)
            {
                case true when button.CustomId is StartButtonId:
                case false when button.CustomId is NextButtonId or StopButtonId:
                    button.Disable();
                    break;
            }
        }

        var embedBuilder = new DiscordEmbedBuilder();

        embedBuilder.WithColor(MessageColor);
        embedBuilder.WithTitle("Turn Order");

        if (turnsCount > 0)
        {
            embedBuilder.WithDescription(descriptionBuilder.ToString());
            embedBuilder.AddField("Name", nameBuilder.ToString(), true);
            embedBuilder.AddField("Value", valueBuilder.ToString(), true);
        }
        else
        {
            embedBuilder.WithDescription(description);
        }

        var messageContentBuilder = new StringBuilder();
        if (trackerStatus is { CurrentTurn: { } })
        {
            messageContentBuilder.Append($"<@{trackerStatus.CurrentTurn.UserId}>, ");
        }

        messageContentBuilder.Append(isPrivate
            ? "This private tracker will be deleted after 1 hour of inactivity."
            : "This tracker will be hidden from chat after 1 hour of inactivity.");

        // we don't have any IDiscordMessageBuilder interface or something like that, so we have to do all this instead
        if (typeof(T) == typeof(DiscordWebhookBuilder))
        {
            var webhookBuilder = new DiscordWebhookBuilder();

            webhookBuilder.WithContent(messageContentBuilder.ToString());
            webhookBuilder.AddEmbed(embedBuilder.Build());
            webhookBuilder.AddComponents(newComponentRows);

            return webhookBuilder as T;
        }

        if (typeof(T) == typeof(DiscordInteractionResponseBuilder))
        {
            var interactionResponseBuilder = new DiscordInteractionResponseBuilder();

            interactionResponseBuilder.WithContent(messageContentBuilder.ToString());
            interactionResponseBuilder.AddEmbed(embedBuilder.Build());
            interactionResponseBuilder.AddComponents(newComponentRows);

            if (isPrivate)
            {
                interactionResponseBuilder.AsEphemeral();
            }

            return interactionResponseBuilder as T;
        }

        if (typeof(T) == typeof(DiscordFollowupMessageBuilder))
        {
            var followupMessageBuilder = new DiscordFollowupMessageBuilder();

            followupMessageBuilder.WithContent(messageContentBuilder.ToString());
            followupMessageBuilder.AddEmbed(embedBuilder.Build());
            followupMessageBuilder.AddComponents(newComponentRows);

            if (isPrivate)
            {
                followupMessageBuilder.AsEphemeral();
            }

            return followupMessageBuilder as T;
        }

        return default;
    }

    private T? BuildTrackerResponse<T>(IEnumerable<DiscordActionRowComponent> components,
        TurnTrackerStatus? trackerStatus = default,
        IEnumerable<Turn>? turns = default, bool isPrivate = false)
        where T : class
    {
        string? description = default;
        var descriptionBuilder = new StringBuilder();
        var nameBuilder = new StringBuilder();
        var valueBuilder = new StringBuilder();
        var turnsArray = turns?.ToArray();
        var entriesCount = turnsArray?.Length;
        var startedTracker = false;
        if (trackerStatus != null)
        {
            if (trackerStatus.CurrentTurn != null)
            {
                startedTracker = true;
                descriptionBuilder.AppendLine(
                    $"Current turn: **{trackerStatus.CurrentTurn.Name}** (<@{trackerStatus.CurrentTurn.UserId}>)");
            }

            if (trackerStatus.NextTurn != null)
            {
                startedTracker = true;
                descriptionBuilder.AppendLine(
                    $"Next turn: **{trackerStatus.NextTurn.Name}** (<@{trackerStatus.NextTurn.UserId}>)");
            }
        }

        if (turnsArray != null && entriesCount > 0)
        {
            foreach (var entry in turnsArray.OrderByDescending(t => t.Value))
            {
                nameBuilder.AppendLine(entry.Name);
                valueBuilder.AppendLine(entry.Value.ToString());
            }
        }
        else
        {
            description = "There aren't currently any entries on this tracker that take a turn!";
        }

        var newComponentRows = components.ToList();
        foreach (var component in newComponentRows.SelectMany(componentRow => componentRow.Components))
        {
            if (component is not DiscordButtonComponent button)
                continue;

            var _ = entriesCount switch
            {
                1 when button.CustomId is StartButtonId or NextButtonId or StopButtonId => button.Disable(),
                0 or null when button.CustomId is EditButtonId or RemoveButtonId or StartButtonId or NextButtonId
                    or StopButtonId => button.Disable(),
                _ => button.Enable()
            };

            switch (startedTracker)
            {
                case true when button.CustomId is StartButtonId:
                case false when button.CustomId is NextButtonId or StopButtonId:
                    button.Disable();
                    break;
            }
        }

        var embedBuilder = new DiscordEmbedBuilder();

        embedBuilder.WithColor(MessageColor);
        embedBuilder.WithTitle("Turn Order");

        if (entriesCount > 0)
        {
            embedBuilder.WithDescription(descriptionBuilder.ToString());
            embedBuilder.AddField("Name", nameBuilder.ToString(), true);
            embedBuilder.AddField("Value", valueBuilder.ToString(), true);
        }
        else
        {
            embedBuilder.WithDescription(description);
        }

        var messageContentBuilder = new StringBuilder();
        if (trackerStatus is { CurrentTurn: { } })
        {
            messageContentBuilder.Append($"<@{trackerStatus.CurrentTurn.UserId}>, ");
        }

        messageContentBuilder.Append(isPrivate
            ? "This private tracker will be deleted after 1 hour of inactivity."
            : "This tracker will be hidden from chat after 1 hour of inactivity.");

        // we don't have any IDiscordMessageBuilder interface or something like that, so we have to do all this instead
        if (typeof(T) == typeof(DiscordWebhookBuilder))
        {
            var webhookBuilder = new DiscordWebhookBuilder();

            webhookBuilder.WithContent(messageContentBuilder.ToString());
            webhookBuilder.AddEmbed(embedBuilder.Build());
            webhookBuilder.AddComponents(newComponentRows);

            return webhookBuilder as T;
        }

        if (typeof(T) == typeof(DiscordInteractionResponseBuilder))
        {
            var interactionResponseBuilder = new DiscordInteractionResponseBuilder();

            interactionResponseBuilder.WithContent(messageContentBuilder.ToString());
            interactionResponseBuilder.AddEmbed(embedBuilder.Build());
            interactionResponseBuilder.AddComponents(newComponentRows);

            if (isPrivate)
            {
                interactionResponseBuilder.AsEphemeral();
            }

            return interactionResponseBuilder as T;
        }

        if (typeof(T) == typeof(DiscordFollowupMessageBuilder))
        {
            var followupMessageBuilder = new DiscordFollowupMessageBuilder();

            followupMessageBuilder.WithContent(messageContentBuilder.ToString());
            followupMessageBuilder.AddEmbed(embedBuilder.Build());
            followupMessageBuilder.AddComponents(newComponentRows);

            if (isPrivate)
            {
                followupMessageBuilder.AsEphemeral();
            }

            return followupMessageBuilder as T;
        }

        return default;
    }

    private Task<Expression?> EvaluateAsync(string expression, CancellationToken cancellationToken = default) =>
        ReceiveFromApiAsync<Expression>(HttpMethod.Post, "/Expression/Evaluate", new { Expression = expression },
            cancellationToken: cancellationToken);

    private Task<TurnTracker> GetTrackerAsync(long trackerId, CancellationToken cancellationToken = default) =>
        ReceiveFromApiAsync<TurnTracker>(HttpMethod.Get, $"/TurnTracker/{trackerId}", needsAuth: true,
            cancellationToken: cancellationToken)!;

    private Task<TurnTracker?> GetTrackerAsync(ulong channelId, ulong userId,
        CancellationToken cancellationToken = default) => ReceiveFromApiAsync<TurnTracker>(HttpMethod.Get,
        $"/TurnTracker?chatId={channelId}&userId={userId}", needsAuth: true,
        cancellationToken: cancellationToken);

    private async Task<TurnTrackerStatus?> GetTrackerStatusAsync(long trackerId,
        CancellationToken cancellationToken = default) => await ReceiveFromApiAsync<TurnTrackerStatus>(HttpMethod.Get,
        $"/TurnTracker/{trackerId}/Status", needsAuth: true, cancellationToken: cancellationToken);

    private Task<IEnumerable<Turn>?> GetTurnsAsync(TurnTracker tracker, ulong? userId = default,
        CancellationToken cancellationToken = default) => ReceiveFromApiAsync<IEnumerable<Turn>>(HttpMethod.Get,
        $"/TurnTracker/{tracker.Id}/Turns{(userId.HasValue && tracker.UserId != userId ? $"?userId={userId}" : string.Empty)}",
        needsAuth: true, cancellationToken: cancellationToken);

    private Task DeleteTrackerAsync(long trackerId, ulong userId) => ExecuteApiAsync(HttpMethod.Delete,
        $"/TurnTracker/{trackerId}?userId={userId}", needsAuth: true);
}