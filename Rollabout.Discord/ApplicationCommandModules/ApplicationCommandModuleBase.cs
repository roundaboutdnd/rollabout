using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text.Encodings.Web;
using System.Text.Json;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using Microsoft.Extensions.Options;
using Rollabout.Contracts.Entities;
using Rollabout.Contracts.Exceptions;

namespace Rollabout.Discord.ApplicationCommandModules;

internal abstract class ApplicationCommandModuleBase : ApplicationCommandModule, IDisposable
{
    private readonly IOptionsMonitor<DiscordApplicationOptions> _applicationOptions;
    private readonly HttpClient _httpClient;

    protected DiscordColor MessageColor { get; }

    protected ApplicationCommandModuleBase(IOptionsMonitor<DiscordApplicationOptions> applicationOptions,
        DiscordColor? discordColor = default)
    {
        _applicationOptions = applicationOptions;
        _httpClient = new HttpClient();
        MessageColor = discordColor ?? DiscordColor.Black;
    }

    protected async Task<T?> ReceiveFromApiAsync<T>(HttpMethod httpMethod, string apiPath, object? body = default,
        bool needsAuth = false, CancellationToken cancellationToken = default)
    {
        var apiEndpoint = _applicationOptions.CurrentValue.ApiEndpoint;
        if (needsAuth)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                _applicationOptions.CurrentValue.ApiToken);
        }

        var response = await _httpClient.SendAsync(new HttpRequestMessage(httpMethod, $"{apiEndpoint}{apiPath}")
        {
            Content = body == null || httpMethod == HttpMethod.Get
                ? default
                : JsonContent.Create(body,
                    options: new JsonSerializerOptions { Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping })
        }, cancellationToken);

        if (cancellationToken.IsCancellationRequested)
            return default;

        if (response.StatusCode == HttpStatusCode.NotFound)
            return default;

        if (response.IsSuccessStatusCode)
            return await response.Content.ReadFromJsonAsync<T>(cancellationToken: cancellationToken);

        var error = await response.Content.ReadFromJsonAsync<ApiError>(cancellationToken: cancellationToken);
        if (error != null)
            throw new RollaboutApiException(error);

        return default;
    }

    protected async Task ExecuteApiAsync(HttpMethod httpMethod, string apiPath, object? body = default,
        bool needsAuth = false, CancellationToken cancellationToken = default)
    {
        var apiEndpoint = _applicationOptions.CurrentValue.ApiEndpoint;
        if (needsAuth)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                _applicationOptions.CurrentValue.ApiToken);
        }

        var response = await _httpClient.SendAsync(new HttpRequestMessage(httpMethod, $"{apiEndpoint}{apiPath}")
            {
                Content = body == null || httpMethod == HttpMethod.Get
                    ? default
                    : JsonContent.Create(body,
                        options: new JsonSerializerOptions { Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping })
            },
            cancellationToken);

        if (cancellationToken.IsCancellationRequested || response.IsSuccessStatusCode)
            return;

        var error = await response.Content.ReadFromJsonAsync<ApiError>(cancellationToken: cancellationToken);
        if (error != null)
            throw new RollaboutApiException(error);
    }

    public void Dispose()
    {
        _httpClient.Dispose();
    }
}