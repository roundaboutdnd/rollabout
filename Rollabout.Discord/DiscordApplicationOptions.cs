using System.ComponentModel.DataAnnotations;
using Rollabout.Contracts.Options;

namespace Rollabout.Discord;

internal sealed class DiscordApplicationOptions : ApplicationOptions
{
    [Required(AllowEmptyStrings = true)] public string ApiEndpoint { get; set; } = null!;

    public string ApiToken { get; set; } = null!;

    public TimeSpan PingInterval { get; set; } = TimeSpan.FromMinutes(1);

    public bool UseAdvancedFeatures { get; set; } = true;

    public Dictionary<string, string>? StatsApiTokens { get; set; }
}