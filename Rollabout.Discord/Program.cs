﻿using DSharpPlus;
using DSharpPlus.Interactivity;
using DSharpPlus.SlashCommands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Rollabout.Contracts.Options;
using Rollabout.Discord;
using Rollabout.Discord.ApplicationCommandModules;
using Rollabout.Discord.Services;

var builder = Host.CreateDefaultBuilder(args);

builder.ConfigureLogging(b => b.AddSimpleConsole(o => o.SingleLine = true));
builder.ConfigureServices((c, s) =>
{
    s.AddConfiguration<DiscordConfiguration>(c.Configuration);
    s.AddConfiguration<InteractivityConfiguration>(c.Configuration);
    s.AddOptions<DiscordApplicationOptions>()
        .Bind(c.Configuration.GetSection(ApplicationOptions.ConfigurationSectionName))
        .ValidateDataAnnotations()
        .ValidateOnStart();

    s.AddSingleton(p => new SlashCommandsConfiguration { Services = p });
    s.AddSingleton(p =>
    {
        var discordConfiguration = p.GetService<DiscordConfiguration>();
        var discordShardedClient = new DiscordShardedClient(discordConfiguration);
        var slashCommandsConfiguration = p.GetService<SlashCommandsConfiguration>();
        if (slashCommandsConfiguration == null)
            return discordShardedClient;

        // this method must be executed synchronously in this particular context, it will spam gateway otherwise
        var slashCommands = discordShardedClient.UseSlashCommandsAsync(slashCommandsConfiguration).Result;
        if (slashCommands == null)
            return discordShardedClient;

        slashCommands.RegisterCommands<ExpressionApplicationCommandModule>();

        var applicationOptions = p.GetService<IOptionsMonitor<DiscordApplicationOptions>>();
        if (applicationOptions == null || !applicationOptions.CurrentValue.UseAdvancedFeatures)
            return discordShardedClient;

        slashCommands.RegisterCommands<TurnTrackerApplicationCommandModule>();

        return discordShardedClient;
    });

    s.AddHostedService<BotBackgroundService>();
    s.AddHostedService<IBotStatsBackgroundService>(p =>
    {
        var applicationOptions = p.GetService<IOptionsMonitor<DiscordApplicationOptions>>();
        var currentOptions = applicationOptions?.CurrentValue;
        return currentOptions?.StatsApiTokens == null || currentOptions.StatsApiTokens.Count == 0 || currentOptions.StatsApiTokens.All(t => string.IsNullOrEmpty(t.Value))
#pragma warning disable CS0618
            ? new BotStatsStubBackgroundService()
#pragma warning restore CS0618
            : new BotStatsBackgroundService(applicationOptions!, p.GetRequiredService<DiscordShardedClient>());
    });
});

var app = builder.Build();

app.Run();