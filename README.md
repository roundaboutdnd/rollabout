# Rollabout

A simple, but powerful dice rolling bot for Discord

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

This is a list of things you need to use the software and how to install them.

* [docker](https://docs.docker.com/engine/install/)
* [docker compose](https://docs.docker.com/compose/install/)

### Installation

1. Clone the repo:
   ```shell
   git clone https://gitlab.com/roundaboutdnd/rollabout.git
   ```
2. Switch to your cloned repo directory:
   ```shell
   cd /path/to/rollabout/repo
   ```
3. If you're going to use database profiles, you have to generate your own API secret:
   ```shell
   # this goes to JWT_TOKEN env variable
   openssl rand -base64 32
   ```
4. Edit `.env` file to set your own options, credentials, tokens, and other configurable stuff.
5. Run `docker compose up`:
   ```shell
   # if you don't need any database related services and bot features
   docker compose --env-file .env --profile simple up --build -d --force-recreate
   
   # if you're going to use PostgreSQL
   docker compose --env-file .env --profile postgresql up --build -d --force-recreate --pull always
   
   # if you're going to use SQLite
   docker compose --env-file .env --profile sqlite up --build -d --force-recreate
   ```
6. If you're using either `postgresql` or `sqlite` profile, you have to generate your own API key:
   ```shell
   # this goes to API_TOKEN env variable
   docker exec <Rollabout API container name> mk-jwt-token
   ```
7. Restart Rollabout services if necessary:
   ```shell
   # this will recreate only updated services
   docker compose --env-file Development.env --profile <profile> up -d
   ```
## Usage

You can start using Rollabout right away, if you've followed Installation steps. Simply add your bot to your own
server and type `/` in chat to get all available commands and options this bot can provide.

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any
contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also
simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request

## License

Distributed under the MIT License. See `LICENSE.md` for more information.
