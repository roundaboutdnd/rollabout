using System;
using System.Collections.Generic;
using System.Text;
using Rollabout.Engine.Dice;
using Rollabout.Engine.Extensions;
using Rollabout.Engine.Modifiers;
using Rollabout.Engine.Options;

namespace Rollabout.Engine;

internal sealed record Roll(Die Die, int? DiceCount = default,
        IEnumerable<ModifierParameters>? ModifierParameters = default)
    : IParsable<Roll>
{
    private static readonly StringBuilder DiceCountBuilder = new();
    private static readonly StringBuilder DieBuilder = new();

    public static Roll Parse(string s, IFormatProvider? provider)
    {
        ClearBuilders();

        if (string.IsNullOrEmpty(s) || string.IsNullOrWhiteSpace(s))
            throw new ArgumentNullException(nameof(s));

        var span = s.AsSpan();
        var modifierStartIndex = -1;
        for (var i = 0; i < span.Length; i++)
        {
            var ch = span[i];
            if (char.IsDigit(ch))
            {
                if (DieBuilder.Length == 0)
                {
                    DiceCountBuilder.Append(ch);
                }
                else
                {
                    DieBuilder.Append(ch);
                }

                continue;
            }

            if ((ch == DefaultParserOptions.DieSeparator && DieBuilder.Length == 0) ||
                ch == DefaultParserOptions.FateDieSymbol)
            {
                DieBuilder.Append(ch);
                continue;
            }

            modifierStartIndex = i;

            break;
        }

        if (!Die.TryParse(DieBuilder.ToString(), null, out var die))
            throw new FormatException();

        if (!int.TryParse(DiceCountBuilder.ToString(), out var diceCount))
        {
            diceCount = -1;
        }

        DiceCountBuilder.Clear();
        DieBuilder.Clear();

        int? actualDiceCount = diceCount == -1 ? null : diceCount;

        // ReSharper disable once ReplaceSliceWithRangeIndexer
        return modifierStartIndex == -1
            ? new Roll(die, actualDiceCount)
            : new Roll(die, actualDiceCount, ModifierExtensions.Parse(span.Slice(modifierStartIndex)));
    }

    public static bool TryParse(string? s, IFormatProvider? provider, out Roll result)
    {
        try
        {
            result = Parse(s ?? string.Empty, provider);
        }
        catch (Exception)
        {
            result = null!;

            return false;
        }

        return true;
    }

    public IEnumerable<FormattedResult> GetValues(ModifierFactory modifierFactory, bool useFormatting = false)
    {
        var result = new List<FormattedResult>();
        for (var _ = 0; _ < (DiceCount ?? 1); _++)
        {
            var rolledValue = Die.GetRolledValue();

            result.Add(new FormattedResult(rolledValue)
            {
                UseAnsiFormatting = useFormatting,
                CriticalSuccessValue = MathExtensions.Equals(rolledValue, Die.SidesCount),
                CriticalFailureValue = MathExtensions.Equals(rolledValue, 1),
                DieType = Die.Type
            });
        }

        if (ModifierParameters == null)
            return result;

        var resultsArray = result.ToArray();
        foreach (var modifierParameters in ModifierParameters)
        {
            modifierParameters.Values = resultsArray;
            modifierParameters.Die = Die;

            var modifier = modifierFactory.GetModifier(modifierParameters.Action);
            if (modifier == null)
                continue;

            resultsArray = modifier.GetModifiedValues(modifierParameters);
        }

        return resultsArray;
    }

    private static void ClearBuilders()
    {
        DiceCountBuilder.Clear();
        DieBuilder.Clear();
    }
}