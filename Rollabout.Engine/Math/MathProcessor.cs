using System;
using System.Collections.Generic;
using System.Text;
using Rollabout.Contracts.Entities;
using Rollabout.Engine.Math.Functions;
using Rollabout.Engine.Modifiers;
using Rollabout.Engine.Options;

namespace Rollabout.Engine.Math;

internal sealed class MathProcessor
{
    private readonly ModifierFactory _modifierFactory;
    private readonly FunctionFactory _functionFactory;

    public MathProcessor(ModifierFactory modifierFactory, FunctionFactory functionFactory)
    {
        _modifierFactory = modifierFactory;
        _functionFactory = functionFactory;
    }

    public ExpressionResult Calculate(ReadOnlySpan<char> data, ref int from,
        char to = DefaultParserOptions.AdditionalTextSeparator)
    {
        if (from >= data.Length || data[from] == to)
            throw new ArgumentException($"Loaded invalid data: {data}");

        var listToMerge = new List<Variable>(16);
        var itemBuilder = new StringBuilder();
        do
        {
            var ch = data[from++];
            if (StillCollecting(itemBuilder.ToString(), ch, to))
            {
                itemBuilder.Append(ch);

                if (from < data.Length && data[from] != to)
                    continue;
            }

            var value = new List<FormattedResult>();
            var function = _functionFactory.GetFunction(itemBuilder.ToString(), ch);
            if (function == null)
            {
                var item = itemBuilder.ToString();
                var separatorIndex = item.IndexOfAny(DefaultParserOptions.AdditionalTextSeparators);

                // ReSharper disable once ReplaceSubstringWithRangeIndexer
                if (separatorIndex > 0 && double.TryParse(item.AsSpan(0, separatorIndex), out var num))
                {
                    value.Add(new FormattedResult(num));
                }
                else if (double.TryParse(item, out num))
                {
                    value.Add(new FormattedResult(num));
                }
                else
                {
                    var roll = Roll.Parse(item, null);
                    if (roll == null)
                        throw new ArgumentException($"Could not parse token [{item}]");

                    value.AddRange(roll.GetValues(_modifierFactory));
                }
            }
            else
            {
                var funcResult = function.GetValue(data, ref from);

                value.Add(new FormattedResult(funcResult.Result));
            }

            var action = ValidAction(ch) ? ch : UpdateAction(data, ref from, ch, to);

            listToMerge.Add(new Variable(value, action));
            itemBuilder.Clear();
        } while (from < data.Length && data[from] != to);

        if (from < data.Length && (data[from] == DefaultParserOptions.CloseArgument || data[from] == to))
        {
            from++;
        }

        var baseVariable = listToMerge[0];
        var index = 1;
        return Merge(baseVariable, ref index, listToMerge);
    }

    private bool StillCollecting(string item, char ch, char to)
    {
        var stopCollecting = to is DefaultParserOptions.CloseArgument or DefaultParserOptions.Eof
            ? DefaultParserOptions.CloseArgument
            : to;

        return (item.Length == 0 &&
                ((MathActions)ch == MathActions.Subtract || ch == DefaultParserOptions.CloseArgument)) ||
               !(ValidAction(ch) || ch == DefaultParserOptions.OpenArgument || ch == stopCollecting);
    }

    private static bool ValidAction(char ch) => Enum.IsDefined((MathActions)ch);

    private static char UpdateAction(ReadOnlySpan<char> item, ref int from, char ch, char to)
    {
        if (from >= item.Length || item[from] == DefaultParserOptions.CloseArgument || item[from] == to)
            return DefaultParserOptions.CloseArgument;

        var index = from;
        var res = ch;
        while (!ValidAction(res) && index < item.Length)
        {
            res = item[index++];
        }

        from = ValidAction(res)
            ? index
            : index > from
                ? index - 1
                : from;

        return res;
    }

    private static ExpressionResult Merge(Variable current, ref int index, IReadOnlyList<Variable> listToMerge,
        bool mergeOneOnly = false)
    {
        while (index < listToMerge.Count)
        {
            var next = listToMerge[index++];
            while (!CanMergeVariables(current, next))
            {
                Merge(next, ref index, listToMerge, true);
            }

            MergeVariables(ref current, next);

            if (mergeOneOnly)
                return new ExpressionResult(listToMerge, current.Value);
        }

        return new ExpressionResult(listToMerge, current.Value);
    }

    private static bool CanMergeVariables(Variable leftVariable, Variable rightVariable) =>
        GetPriority(leftVariable.Action) >= GetPriority(rightVariable.Action);

    private static int GetPriority(char action) => (MathActions)action switch
    {
        MathActions.Power => 4,
        MathActions.Multiply or MathActions.Divide or MathActions.Modulus => 3,
        MathActions.Add or MathActions.Subtract => 2,
        _ => 0
    };

    private static void MergeVariables(ref Variable leftVariable, Variable rightVariable)
    {
        leftVariable = (MathActions)leftVariable.Action switch
        {
            MathActions.Add => leftVariable + rightVariable,
            MathActions.Subtract => leftVariable - rightVariable,
            MathActions.Multiply => leftVariable * rightVariable,
            MathActions.Divide => leftVariable / rightVariable,
            MathActions.Power => new Variable(leftVariable.Values, rightVariable.Action)
            {
                Value = System.Math.Pow(leftVariable.Value, rightVariable.Value)
            },
            MathActions.Modulus => leftVariable % rightVariable,
            _ => leftVariable
        };
    }
}