using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Rollabout.Engine.Dice;

namespace Rollabout.Engine.Math;

/// <summary>
/// Variable for math processing.
/// </summary>
public sealed class Variable : IAdditionOperators<Variable, Variable, Variable>,
    ISubtractionOperators<Variable, Variable, Variable>, IMultiplyOperators<Variable, Variable, Variable>,
    IDivisionOperators<Variable, Variable, Variable>, IModulusOperators<Variable, Variable, Variable>
{
    /// <summary>
    /// A collection of values.
    /// </summary>
    public ICollection<FormattedResult> Values { get; }

    /// <summary>
    /// Sum of all values.
    /// </summary>
    public double Value { get; set; }

    /// <summary>
    /// Current math action.
    /// </summary>
    public char Action { get; private set; }

    /// <summary>
    /// Original parsed math action.
    /// </summary>
    public char OriginalAction { get; }

    /// <summary>
    /// Whether this variable is a dice roll.
    /// </summary>
    public bool IsDiceRoll { get; }

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="values">A collection of values</param>
    /// <param name="action">Math action</param>
    public Variable(ICollection<FormattedResult> values, char action)
    {
        Values = values;
        OriginalAction = Action = action;
        Value = Values.Where(r => !r.Skip).Sum(r => r.Result);
        IsDiceRoll = Values.All(r => r.DieType != DieTypes.None);
    }

    public static Variable operator +(Variable left, Variable right)
    {
        left.Value += right.Value;
        left.Action = right.Action;

        return left;
    }

    public static Variable operator -(Variable left, Variable right)
    {
        left.Value -= right.Value;
        left.Action = right.Action;

        return left;
    }

    public static Variable operator *(Variable left, Variable right)
    {
        left.Value *= right.Value;
        left.Action = right.Action;

        return left;
    }

    public static Variable operator /(Variable left, Variable right)
    {
        left.Value /= right.Value;
        left.Action = right.Action;

        return left;
    }

    public static Variable operator %(Variable left, Variable right)
    {
        left.Value %= right.Value;
        left.Action = right.Action;

        return left;
    }
}