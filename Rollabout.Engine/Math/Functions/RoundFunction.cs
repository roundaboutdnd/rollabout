using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Rollabout.Engine.Options;

namespace Rollabout.Engine.Math.Functions;

internal sealed class RoundFunction : FunctionBase
{
    public override string Name => "round";

    public RoundFunction(IServiceProvider serviceProvider)
        : base(serviceProvider)
    {
    }

    public override ExpressionResult GetValue(ReadOnlySpan<char> data, ref int from)
    {
        var expressionResult = ServiceProvider.GetService<MathProcessor>()
            ?.Calculate(data, ref from, DefaultParserOptions.CloseArgument);

        if (expressionResult == null)
            return new ExpressionResult(Enumerable.Empty<Variable>(), double.NaN);

        expressionResult.Result = System.Math.Round(expressionResult.Result);

        return expressionResult;
    }
}