using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Rollabout.Engine.Options;

namespace Rollabout.Engine.Math.Functions;

internal sealed class IdentityFunction : FunctionBase
{
    public override string Name => string.Empty;

    public IdentityFunction(IServiceProvider serviceProvider)
        : base(serviceProvider)
    {
    }

    public override ExpressionResult GetValue(ReadOnlySpan<char> data, ref int from)
    {
        var expressionResult = ServiceProvider.GetService<MathProcessor>()
            ?.Calculate(data, ref from, DefaultParserOptions.CloseArgument);

        return expressionResult ?? new ExpressionResult(Enumerable.Empty<Variable>(), double.NaN);
    }
}