using System;

namespace Rollabout.Engine.Math.Functions;

/// <summary>
/// Interface for math functions implementations.
/// </summary>
public interface IFunction
{
    /// <summary>
    /// Function name that will be used in math expressions.
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Parse a string and compute function value.
    /// </summary>
    /// <param name="data">Expression string for parsing</param>
    /// <param name="from">Starting point in string</param>
    /// <returns></returns>
    ExpressionResult GetValue(ReadOnlySpan<char> data, ref int from);
}