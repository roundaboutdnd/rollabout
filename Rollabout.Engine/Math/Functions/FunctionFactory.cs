using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Rollabout.Engine.Options;

namespace Rollabout.Engine.Math.Functions;

internal sealed class FunctionFactory
{
    private readonly IEnumerable<IFunction> _functions;

    public FunctionFactory(IServiceProvider serviceProvider)
    {
        _functions = serviceProvider.GetServices<IFunction>();
    }

    public IFunction? GetFunction(string name, char? ch = default) =>
        string.IsNullOrEmpty(name) && ch == DefaultParserOptions.OpenArgument
            ? _functions.FirstOrDefault(f => f is IdentityFunction)
            : _functions.FirstOrDefault(f => f.Name == name);
}