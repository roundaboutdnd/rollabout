using System;

namespace Rollabout.Engine.Math.Functions;

internal abstract class FunctionBase : IFunction
{
    protected IServiceProvider ServiceProvider { get; }

    protected FunctionBase(IServiceProvider serviceProvider)
    {
        ServiceProvider = serviceProvider;
    }

    public abstract string Name { get; }

    public abstract ExpressionResult GetValue(ReadOnlySpan<char> data, ref int from);
}