using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rollabout.Engine.Dice;
using Rollabout.Engine.Options;

namespace Rollabout.Engine.Math;

/// <summary>
/// Math expression result.
/// </summary>
public sealed class ExpressionResult
{
    /// <summary>
    /// Math expression variables.
    /// </summary>
    public IEnumerable<Variable> Variables { get; }

    /// <summary>
    /// Math expression result.
    /// </summary>
    public double Result { get; set; }

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="variables">Math expression variables</param>
    /// <param name="result">Math expression result</param>
    public ExpressionResult(IEnumerable<Variable> variables, double result)
    {
        Variables = variables;
        Result = result;
    }

    public override string? ToString()
    {
        if (!Variables.Any())
            return base.ToString();

        if (Variables.Count() == 1 && Variables.All(v => v.Values.Count == 1))
            return Variables.SelectMany(v => v.Values).FirstOrDefault()?.ToString() ?? base.ToString();

        var sb = new StringBuilder();
        foreach (var variable in Variables)
        {
            if (variable.IsDiceRoll)
            {
                sb.Append(DefaultParserOptions.OpenArgument);
            }

            sb.AppendJoin(
                variable.Values.Any(v => v.DieType == DieTypes.FateDie)
                    ? string.Empty
                    : DefaultParserOptions.ExpressionsSeparator.ToString(), variable.Values);

            if (variable.IsDiceRoll)
            {
                sb.Append(DefaultParserOptions.CloseArgument);
            }

            if (variable.OriginalAction != DefaultParserOptions.CloseArgument)
            {
                sb.Append(variable.OriginalAction);
            }
        }

        return sb.ToString();
    }
}