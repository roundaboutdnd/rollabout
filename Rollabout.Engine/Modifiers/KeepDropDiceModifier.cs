using System;
using System.Linq;
using Rollabout.Engine.Extensions;

namespace Rollabout.Engine.Modifiers;

[ModifierActions("k", "d", "kh", "dl", "kl", "dh", "adv", "dis")]
internal sealed record KeepDropDiceModifier : ModifierBase
{
    public override FormattedResult[] GetModifiedValues(ModifierParameters parameters)
    {
        var values = parameters.Values;
        var targetNumber = parameters.TargetNumber;
        if (!targetNumber.HasValue)
        {
            if (parameters.Action is "adv" or "dis")
            {
                targetNumber = 1;
            }
            else
                throw new ArgumentNullException(nameof(parameters.TargetNumber));
        }

        var keep = parameters.Action.Contains('k') || parameters.Action is "adv" or "dis";
        var useHighest = keep switch
        {
            true when parameters.Action.Contains('l') || parameters.Action == "dis" => false,
            false when parameters.Action.Contains('h') || parameters.Action == "adv" => true,
            true => true,
            false => false
        };

        var orderedValues = useHighest
            ? values.OrderByDescending(v => v.Result)
            : values.OrderBy(v => v.Result);

        var newValues = (keep
            ? orderedValues.Take((int)targetNumber)
            : orderedValues.Skip((int)targetNumber)).Select(r => r.Result).ToArray();

        foreach (var value in values)
        {
            value.Skip = newValues.All(x => !MathExtensions.Equals(x, value.Result));
        }

        return values;
    }
}