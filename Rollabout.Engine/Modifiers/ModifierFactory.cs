using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace Rollabout.Engine.Modifiers;

internal sealed class ModifierFactory
{
    private readonly Dictionary<string, IModifier> _modifiers = new();

    public ModifierFactory(IServiceProvider serviceProvider)
    {
        var registeredModifiers = serviceProvider.GetServices<IModifier>();
        foreach (var modifier in registeredModifiers)
        {
            var type = modifier.GetType();
            var actions = type.GetCustomAttribute<ModifierActionsAttribute>()?.Actions;
            if (actions == null)
                continue;

            foreach (var action in actions)
            {
                _modifiers.TryAdd(action, modifier);
            }
        }
    }

    public IModifier? GetModifier(string action) => _modifiers.GetValueOrDefault(action);
}