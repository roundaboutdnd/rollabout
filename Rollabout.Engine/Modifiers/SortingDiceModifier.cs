using System.Linq;

namespace Rollabout.Engine.Modifiers;

[ModifierActions("s", "sd")]
internal sealed record SortingDiceModifier : ModifierBase
{
    public override FormattedResult[] GetModifiedValues(ModifierParameters parameters) =>
        (parameters.Action == "sd"
            ? parameters.Values.OrderByDescending(v => v.Result)
            : parameters.Values.OrderBy(v => v.Result)).ToArray();
}