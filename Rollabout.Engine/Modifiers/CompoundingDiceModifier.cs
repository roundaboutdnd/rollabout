using System;
using System.Collections.Generic;
using System.Linq;
using Rollabout.Engine.Extensions;

namespace Rollabout.Engine.Modifiers;

[ModifierActions("!!")]
internal sealed record CompoundingDiceModifier : ModifierBase
{
    public override FormattedResult[] GetModifiedValues(ModifierParameters parameters)
    {
        if (parameters.Die == null)
            throw new ArgumentNullException(nameof(parameters.Die));

        var explodedValues = new List<FormattedResult>();
        foreach (var value in parameters.Values)
        {
            if (!CheckValueAgainstTarget(value.Result, parameters))
            {
                explodedValues.Add(value);
                continue;
            }

            var explodedValue = value.Result;
            var compoundValues = new List<double> { explodedValue };
            do
            {
                explodedValue = parameters.Die.GetRolledValue();

                compoundValues.Add(explodedValue);
            } while (CheckValueAgainstTarget(explodedValue, parameters));

            var compoundValue = compoundValues.Sum();

            explodedValues.Add(new FormattedResult(compoundValue, value)
            {
                CriticalSuccessValue = MathExtensions.Equals(compoundValue, parameters.Die.SidesCount),
                CriticalFailureValue = MathExtensions.Equals(compoundValue, 1)
            });
        }

        return explodedValues.ToArray();
    }
}