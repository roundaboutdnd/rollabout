using System;
using System.Linq;

namespace Rollabout.Engine.Modifiers;

[ModifierActions("cs", "cf")]
internal sealed record CriticalPointModifier : ModifierBase
{
    public override FormattedResult[] GetModifiedValues(ModifierParameters parameters)
    {
        var values = parameters.Values;
        if (parameters.Die == null)
            throw new ArgumentNullException(nameof(parameters.Die));

        var isFailure = parameters.Action == "cf";
        foreach (var value in values.Where(v => CheckValueAgainstTarget(v.Result, parameters)))
        {
            value.CriticalSuccessValue = !isFailure;
            value.CriticalFailureValue = isFailure;
        }

        return values;
    }
}