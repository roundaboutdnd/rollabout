namespace Rollabout.Engine.Modifiers;

/// <summary>
/// Interface for dice roll modifiers implementations.
/// </summary>
public interface IModifier
{
    /// <summary>
    /// Compute values with parameters.
    /// </summary>
    /// <param name="parameters">Modifier parameters</param>
    /// <returns></returns>
    FormattedResult[] GetModifiedValues(ModifierParameters parameters);
}