using System;
using System.Collections.Generic;
using Rollabout.Engine.Extensions;

namespace Rollabout.Engine.Modifiers;

[ModifierActions("r", "ro", "rodd", "reven")]
internal sealed record RerollingDiceModifier : ModifierBase
{
    private bool _rerollOdd;
    private bool _rerollEven;

    public override FormattedResult[] GetModifiedValues(ModifierParameters parameters)
    {
        if (parameters.Die == null)
            throw new ArgumentNullException(nameof(parameters.Die));

        _rerollOdd = parameters.Action == "rodd";
        _rerollEven = parameters.Action == "reven";

        var rerollOnce = parameters.Action == "ro";
        var rerolledValues = new List<FormattedResult>();
        foreach (var value in parameters.Values)
        {
            if (!CheckValueAgainstTarget(value.Result, parameters))
            {
                rerolledValues.Add(value);
                continue;
            }

            value.Skip = true;

            rerolledValues.Add(value);

            bool check;
            do
            {
                var rerolledValue = parameters.Die.GetRolledValue();

                check = CheckValueAgainstTarget(rerolledValue, parameters);

                rerolledValues.Add(new FormattedResult(rerolledValue, value)
                {
                    CriticalSuccessValue = MathExtensions.Equals(rerolledValue, parameters.Die.SidesCount),
                    CriticalFailureValue = MathExtensions.Equals(rerolledValue, 1),
                    Skip = check && !rerollOnce
                });
            } while (check && !rerollOnce);
        }

        return rerolledValues.ToArray();
    }

    protected override bool CheckValueAgainstTarget(double value, ModifierParameters parameters)
    {
        if (_rerollOdd)
            return value % 2 != 0;

        if (_rerollEven)
            return value % 2 == 0;

        return base.CheckValueAgainstTarget(value, parameters);
    }
}