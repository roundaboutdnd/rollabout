using System;
using System.Collections.Generic;

namespace Rollabout.Engine.Modifiers;

[AttributeUsage(AttributeTargets.Class)]
internal sealed class ModifierActionsAttribute : Attribute
{
    public IEnumerable<string> Actions { get; }

    public ModifierActionsAttribute(params string[] actions) => Actions = actions;
}