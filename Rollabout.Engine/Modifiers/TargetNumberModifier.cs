namespace Rollabout.Engine.Modifiers;

[ModifierActions("", "f")]
internal sealed record TargetNumberModifier : ModifierBase
{
    public override FormattedResult[] GetModifiedValues(ModifierParameters parameters)
    {
        var values = parameters.Values;
        var countFailures = parameters.Action == "f";
        foreach (var value in values)
        {
            value.IsTargetNumberResult = true;

            if (!CheckValueAgainstTarget(value.Result, parameters))
                continue;

            value.IsTargetNumberSuccess = !countFailures;
            value.IsTargetNumberFailure = countFailures;
        }

        return values;
    }
}