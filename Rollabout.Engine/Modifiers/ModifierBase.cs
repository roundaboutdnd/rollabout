using System;
using Rollabout.Engine.Extensions;

namespace Rollabout.Engine.Modifiers;

internal abstract record ModifierBase : IModifier
{
    public abstract FormattedResult[] GetModifiedValues(ModifierParameters parameters);

    protected virtual bool CheckValueAgainstTarget(double value, ModifierParameters parameters)
    {
        var targetNumber = parameters.TargetNumber ?? parameters.Die?.SidesCount;
        if (!targetNumber.HasValue)
            return false;

        return parameters.CompareOperation switch
        {
            CompareOperations.EqualsTo => MathExtensions.Equals(value, targetNumber),
            CompareOperations.GreaterThan => value > targetNumber,
            CompareOperations.LessThan => value < targetNumber,
            CompareOperations.GreaterThanOrEqualsTo => value >= targetNumber,
            CompareOperations.LessThanOrEqualsTo => value <= targetNumber,
            CompareOperations.NotEqualsTo => !MathExtensions.Equals(value, targetNumber),
            _ => throw new ArgumentOutOfRangeException(nameof(parameters.CompareOperation))
        };
    }
}