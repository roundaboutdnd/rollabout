using System;
using System.Collections.Generic;
using Rollabout.Engine.Extensions;

namespace Rollabout.Engine.Modifiers;

[ModifierActions("!p")]
internal sealed record PenetratingDiceModifier : ModifierBase
{
    public override FormattedResult[] GetModifiedValues(ModifierParameters parameters)
    {
        if (parameters.Die == null)
            throw new ArgumentNullException(nameof(parameters.Die));

        var explodedValues = new List<FormattedResult>();
        foreach (var value in parameters.Values)
        {
            explodedValues.Add(value);

            if (!CheckValueAgainstTarget(value.Result, parameters))
                continue;

            double explodedValue;
            do
            {
                explodedValue = parameters.Die.GetRolledValue() - 1;

                explodedValues.Add(new FormattedResult(explodedValue, value)
                {
                    CriticalSuccessValue = MathExtensions.Equals(explodedValue, parameters.Die.SidesCount),
                    CriticalFailureValue = MathExtensions.Equals(explodedValue, 1)
                });
            } while (CheckValueAgainstTarget(explodedValue + 1, parameters));
        }

        return explodedValues.ToArray();
    }
}