using Rollabout.Engine.Dice;

namespace Rollabout.Engine.Modifiers;

/// <summary>
/// Modifier parameters.
/// </summary>
public sealed record ModifierParameters
{
    /// <summary>
    /// Modifier action string.
    /// </summary>
    public string Action { get; init; } = null!;

    /// <summary>
    /// Compare operation if it exists.
    /// </summary>
    public CompareOperations? CompareOperation { get; init; }

    /// <summary>
    /// Target number if it exists.
    /// </summary>
    public double? TargetNumber { get; init; }

    /// <summary>
    /// Values for evaluation.
    /// </summary>
    public FormattedResult[] Values { get; set; } = null!;

    /// <summary>
    /// Die object.
    /// </summary>
    public Die? Die { get; set; }
}