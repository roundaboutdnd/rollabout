using System.Linq;
using Rollabout.Engine.Extensions;

namespace Rollabout.Engine.Modifiers;

[ModifierActions("mt")]
internal sealed record DiceMatchingModifier : ModifierBase
{
    public override FormattedResult[] GetModifiedValues(ModifierParameters parameters)
    {
        var values = parameters.Values;
        var matches = values
            .GroupBy(r => r.Result)
            .Where(v => v.Count() >= (parameters.TargetNumber ?? 2))
            .Select(v => v.Key)
            .ToArray();

        foreach (var value in values)
        {
            value.IsMatchResult = true;

            if (matches.All(m => !MathExtensions.Equals(m, value.Result)))
                continue;

            value.IsTargetNumberResult = true;
        }

        return values;
    }
}