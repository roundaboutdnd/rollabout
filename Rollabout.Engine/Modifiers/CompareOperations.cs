namespace Rollabout.Engine.Modifiers;

/// <summary>
/// Compare operations.
/// </summary>
public enum CompareOperations
{
    /// <summary>
    /// Equals-to (=, ==).
    /// </summary>
    EqualsTo,
    
    /// <summary>
    /// Greater-than (>).
    /// </summary>
    GreaterThan,
    
    /// <summary>
    /// Less-than (<![CDATA[<]]>).
    /// </summary>
    LessThan,
    
    /// <summary>
    /// Greater-than or equals-to (>=).
    /// </summary>
    GreaterThanOrEqualsTo,
    
    /// <summary>
    /// Less-than or equals-to (<![CDATA[<=]]>).
    /// </summary>
    LessThanOrEqualsTo,
    
    /// <summary>
    /// Not equals-to (!=, <![CDATA[<>]]>).
    /// </summary>
    NotEqualsTo
}