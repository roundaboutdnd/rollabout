namespace Rollabout.Engine.Options;

internal static class DefaultParserOptions
{
    public const char DieSeparator = 'd';
    public const char FateDieSymbol = 'F';
    public const char AdditionalTextSeparator = '\\';
    public const char OpenExpressionGroup = '{';
    public const char CloseExpressionGroup = '}';
    public const char OpenArgument = '(';
    public const char CloseArgument = ')';
    public const char ExpressionsSeparator = '+';
    public const char Eof = '\0';
    public static readonly char[] CompareOperationsSymbols = { '<', '=', '>' };
    public static readonly char[] AdditionalTextSeparators = { AdditionalTextSeparator, Whitespace };
    public static readonly char[] ExpressionGroupSeparators = { OpenExpressionGroup, CloseExpressionGroup, Comma };

    private const char Whitespace = ' ';
    private const char Comma = ',';
}