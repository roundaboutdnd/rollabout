using System;
using System.Globalization;
using System.Text;
using Rollabout.Engine.Dice;

namespace Rollabout.Engine;

/// <summary>
/// Result wrapper for additional formatting.
/// </summary>
public sealed class FormattedResult
{
    /// <summary>
    /// Numeric value of result.
    /// </summary>
    public double Result { get; }

    /// <summary>
    /// Whether to use ANSI formatting.
    /// </summary>
    public bool UseAnsiFormatting { get; set; }

    /// <summary>
    /// Whether this result counts as critical success.
    /// </summary>
    public bool CriticalSuccessValue { get; set; }

    /// <summary>
    /// Whether this result counts as critical failure.
    /// </summary>
    public bool CriticalFailureValue { get; set; }

    /// <summary>
    /// Whether to skip this result for further processing.
    /// </summary>
    public bool Skip { get; set; }

    /// <summary>
    /// Die type of this result.
    /// </summary>
    public DieTypes DieType { get; init; }

    /// <summary>
    /// Whether this result has a Target Number Modifier.
    /// </summary>
    public bool IsTargetNumberResult { get; set; }

    /// <summary>
    /// Whether this result counts as success (Target Number Modifier).
    /// </summary>
    public bool IsTargetNumberSuccess { get; set; }

    /// <summary>
    /// Whether this result counts as failure (Target Number Modifier).
    /// </summary>
    public bool IsTargetNumberFailure { get; set; }

    /// <summary>
    /// Whether this result has a Dice Match Modifier.
    /// </summary>
    public bool IsMatchResult { get; set; }

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="result">Numeric value of result</param>
    public FormattedResult(double result) => Result = result;

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="newValue">Numeric value of result</param>
    /// <param name="oldResult">Old FormattingResult to copy its properties onto a new result</param>
    public FormattedResult(double newValue, FormattedResult oldResult)
    {
        Result = newValue;
        UseAnsiFormatting = oldResult.UseAnsiFormatting;
        CriticalSuccessValue = oldResult.CriticalSuccessValue;
        CriticalFailureValue = oldResult.CriticalFailureValue;
        Skip = oldResult.Skip;
        DieType = oldResult.DieType;
        IsTargetNumberResult = oldResult.IsTargetNumberResult;
        IsTargetNumberSuccess = oldResult.IsTargetNumberSuccess;
        IsTargetNumberFailure = oldResult.IsTargetNumberFailure;
        IsMatchResult = oldResult.IsMatchResult;
    }

    public override string ToString()
    {
        if (DieType == DieTypes.FateDie)
            return Result switch
            {
                -1 => "-",
                0 => "0",
                1 => "+",
                _ => throw new ArgumentOutOfRangeException()
            };

        if (!UseAnsiFormatting)
            return Result.ToString(CultureInfo.InvariantCulture);

        var sb = new StringBuilder();
        if (CriticalSuccessValue)
        {
            sb.Append(Skip ? "\u001b[0;32m" : "\u001b[1;32m");
            sb.Append(Result);
            sb.Append("\u001b[0m");
        }
        else if (CriticalFailureValue)
        {
            sb.Append(Skip ? "\u001b[0;31m" : "\u001b[1;31m");
            sb.Append(Result);
            sb.Append("\u001b[0m");
        }
        else if (Skip)
        {
            sb.Append("\u001b[30m");
            sb.Append(Result);
            sb.Append("\u001b[0m");
        }
        else
        {
            sb.Append(Result);
        }

        return sb.ToString();
    }
}