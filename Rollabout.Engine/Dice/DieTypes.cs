namespace Rollabout.Engine.Dice;

/// <summary>
/// Available dice types.
/// </summary>
public enum DieTypes
{
    /// <summary>
    /// No die.
    /// </summary>
    None,

    /// <summary>
    /// Traditional die (aka d4, d6, d8...).
    /// </summary>
    TraditionalDie,

    /// <summary>
    /// Fate/Fudge die.
    /// </summary>
    FateDie
}