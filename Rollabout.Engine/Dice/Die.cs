using System;
using System.Security.Cryptography;
using Rollabout.Engine.Options;

namespace Rollabout.Engine.Dice;

/// <summary>
/// Dice representation.
/// </summary>
/// <param name="Type">Type of die</param>
/// <param name="SidesCount">Sides count</param>
public record Die(DieTypes Type, double SidesCount) : IParsable<Die>
{
    public static Die Parse(string s, IFormatProvider? provider)
    {
        var span = s.AsSpan();
        if (span.IsEmpty || span.IsWhiteSpace())
            throw new ArgumentNullException(nameof(s));

        if (span[0] != DefaultParserOptions.DieSeparator)
            throw new FormatException();

        if (span is [_, DefaultParserOptions.FateDieSymbol])
            return new Die(DieTypes.FateDie, 3);

        // ReSharper disable once ReplaceSliceWithRangeIndexer
        if (int.TryParse(span.Slice(1), out var sidesCount))
            return new Die(DieTypes.TraditionalDie, sidesCount);

        throw new FormatException();
    }

    public static bool TryParse(string? s, IFormatProvider? provider, out Die result)
    {
        try
        {
            result = Parse(s ?? string.Empty, provider);
        }
        catch (Exception)
        {
            result = null!;

            return false;
        }

        return true;
    }

    /// <summary>
    /// Makes a dice roll.
    /// </summary>
    /// <returns>Rolled result.</returns>
    public double GetRolledValue() => MapRolledValueByDieType(RandomNumberGenerator.GetInt32(1, (int)(SidesCount + 1)));

    private int MapRolledValueByDieType(int rolledValue) => Type == DieTypes.FateDie
        ? rolledValue switch
        {
            1 => -1,
            2 => 0,
            3 => 1,
            _ => throw new ArgumentOutOfRangeException(nameof(rolledValue))
        }
        : rolledValue;
}