using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rollabout.Contracts.Entities;
using Rollabout.Contracts.Handlers;
using Rollabout.Engine.Dice;
using Rollabout.Engine.Extensions;
using Rollabout.Engine.Math;
using Rollabout.Engine.Modifiers;
using Rollabout.Engine.Options;

namespace Rollabout.Engine.Handlers;

internal sealed class ExpressionHandler : IExpressionHandler
{
    private readonly ModifierFactory _modifierFactory;
    private readonly MathProcessor _mathProcessor;

    public ExpressionHandler(ModifierFactory modifierFactory, MathProcessor mathProcessor)
    {
        _modifierFactory = modifierFactory;
        _mathProcessor = mathProcessor;
    }

    [SuppressMessage("ReSharper", "ReplaceSliceWithRangeIndexer")]
    public Task<Expression> EvaluateAsync(Expression expression) => Task.Run(() =>
    {
        string? additionalText = default;
        var originalExpression = expression.OriginalString.AsSpan();
        var separatorIndex = originalExpression.LastIndexOf(DefaultParserOptions.AdditionalTextSeparator);
        if (separatorIndex > 0)
        {
            additionalText = originalExpression.Slice(separatorIndex)
                .Trim(DefaultParserOptions.AdditionalTextSeparators).ToString();
        }

        var groupCloseIndex = originalExpression.LastIndexOf(DefaultParserOptions.CloseExpressionGroup);
        var cleanExpression =
            groupCloseIndex == -1 ? originalExpression : originalExpression.Slice(0, groupCloseIndex + 1);
        var modifierParametersList = new List<ModifierParameters>();
        if (groupCloseIndex > 0)
        {
            var parsedModifiers = ModifierExtensions.Parse(originalExpression.Slice(
                groupCloseIndex + 1,
                originalExpression.Length - (separatorIndex > 0 ? separatorIndex : groupCloseIndex) - 1));

            if (parsedModifiers != null)
            {
                modifierParametersList.AddRange(parsedModifiers);
            }
        }

        var tokens = cleanExpression.ToString().Split(DefaultParserOptions.ExpressionGroupSeparators,
            StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);

        var isTargetNumberResult = false;
        var isMatchResult = false;
        if (cleanExpression.SequenceEqual(tokens.FirstOrDefault()))
        {
            var from = 0;
            var expressionResult =
                _mathProcessor.Calculate(cleanExpression.Trim(DefaultParserOptions.ExpressionGroupSeparators),
                    ref from);
            foreach (var value in expressionResult.Variables.SelectMany(v => v.Values))
            {
                value.UseAnsiFormatting = expression.UseAnsiFormatting;
            }

            var formattedResults = expressionResult.Variables.SelectMany(v => v.Values).ToArray();
            var actualResult = expressionResult.Result;
            if (formattedResults.Any(r => r is { IsTargetNumberResult: true, IsMatchResult: false }))
            {
                var successes = formattedResults.Where(r => r is { IsTargetNumberResult: true, IsMatchResult: false })
                    .ToList();

                actualResult = successes.Count(r => r.IsTargetNumberSuccess);

                if (actualResult > 0)
                {
                    actualResult -= successes.Count(r => r.IsTargetNumberFailure);
                }

                isTargetNumberResult = true;
            }
            else if (formattedResults.Any(r => r is { IsTargetNumberResult: true, IsMatchResult: true }))
            {
                var matches = formattedResults.Where(r => r is { IsTargetNumberResult: true, IsMatchResult: true });
                actualResult = matches.Select(r => r.Result).Distinct().Count();
                isMatchResult = true;
            }

            return new Expression
            {
                OriginalString = expression.OriginalString,
                UseAnsiFormatting = expression.UseAnsiFormatting,
                ResultString = expressionResult.ToString() ?? string.Empty,
                ResultValue = actualResult,
                IsTargetNumberResult = isTargetNumberResult,
                IsMatchResult = isMatchResult,
                ResultAdditionalText = additionalText
            };
        }

        var results = new List<ExpressionResult>();
        foreach (var token in tokens)
        {
            if (string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token))
                continue;

            var tokenSpan = token.AsSpan();
            var from = 0;
            var result = _mathProcessor.Calculate(tokenSpan, ref from);
            foreach (var value in result.Variables.SelectMany(v => v.Values))
            {
                value.UseAnsiFormatting = expression.UseAnsiFormatting;
            }

            results.Add(result);
        }

        FormattedResult[] groupResults;
        if (results.Count == 1)
        {
            groupResults = GetModifiedResults(results.First().Variables.SelectMany(v => v.Values),
                modifierParametersList);
        }
        else
        {
            groupResults = GetModifiedResults(results.Select(r => new FormattedResult(r.Result)),
                modifierParametersList);

            for (var i = 0; i < groupResults.Length; i++)
            {
                var result = groupResults[i];
                var actualResults = results[i].Variables.SelectMany(v => v.Values);
                foreach (var actualResult in actualResults)
                {
                    actualResult.IsTargetNumberResult =
                        result?.IsTargetNumberResult ?? actualResult.IsTargetNumberResult;

                    actualResult.IsTargetNumberFailure =
                        result?.IsTargetNumberFailure ?? actualResult.IsTargetNumberFailure;
                    actualResult.IsTargetNumberSuccess =
                        result?.IsTargetNumberSuccess ?? actualResult.IsTargetNumberSuccess;
                    actualResult.Skip = result?.Skip ?? actualResult.Skip;
                }
            }
        }

        var sb = new StringBuilder();

        sb.Append(DefaultParserOptions.OpenExpressionGroup);

        foreach (var resultString in results.Select(result => result.ToString()))
        {
            sb.Append(resultString);
            sb.Append(DefaultParserOptions.ExpressionsSeparator);
        }

        sb.Remove(sb.Length - 1, 1);
        sb.Append(DefaultParserOptions.CloseExpressionGroup);

        var groupFormattedResults = groupResults.Length > 0
            ? groupResults
            : results.SelectMany(r => r.Variables).SelectMany(v => v.Values).ToArray();

        var groupResult = groupFormattedResults.Where(r => !r.Skip).Sum(r => r.Result);
        if (groupFormattedResults.Any(r => r is { IsTargetNumberResult: true, IsMatchResult: false }))
        {
            var successes = groupFormattedResults
                .Where(r => r is { IsTargetNumberResult: true, IsMatchResult: false }).ToList();

            groupResult = successes.Count(r => r.IsTargetNumberSuccess);

            if (groupResult > 0)
            {
                groupResult -= successes.Count(r => r.IsTargetNumberFailure);
            }

            isTargetNumberResult = true;
        }
        else if (groupFormattedResults.Any(r => r is { IsTargetNumberResult: true, IsMatchResult: true }))
        {
            var matches =
                groupFormattedResults.Where(r => r is { IsTargetNumberResult: true, IsMatchResult: true });
            groupResult = matches.Select(r => r.Result).Distinct().Count();
            isMatchResult = true;
        }

        return new Expression
        {
            OriginalString = expression.OriginalString,
            UseAnsiFormatting = expression.UseAnsiFormatting,
            ResultString = sb.ToString(),
            ResultValue = groupResult,
            IsTargetNumberResult = isTargetNumberResult,
            IsMatchResult = isMatchResult,
            ResultAdditionalText = additionalText
        };
    });

    public Task<CoinFlips> FlipCoinsAsync(CoinFlips coinFlips) => Task.Run(() =>
    {
        var roll = new Roll(new Die(DieTypes.TraditionalDie, 2), coinFlips.CoinsCount);
        var results = roll.GetValues(_modifierFactory).ToList();
        return new CoinFlips
        {
            CoinsCount = coinFlips.CoinsCount,
            HeadsCount = results.Count(r => MathExtensions.Equals(r.Result, 2)),
            TailsCount = results.Count(r => MathExtensions.Equals(r.Result, 1)),
        };
    });

    private FormattedResult[] GetModifiedResults(IEnumerable<FormattedResult> results,
        IEnumerable<ModifierParameters> modifierParametersCollection)
    {
        var modifiedValues = results.ToArray();
        foreach (var modifierParameters in modifierParametersCollection)
        {
            modifierParameters.Values = modifiedValues;

            var modifier = _modifierFactory.GetModifier(modifierParameters.Action);
            if (modifier == null)
                continue;

            if (modifier is not (KeepDropDiceModifier or TargetNumberModifier))
                throw new InvalidOperationException(
                    $"Group rolls can only use {nameof(KeepDropDiceModifier)} or {nameof(TargetNumberModifier)} modifiers");

            modifiedValues = modifier.GetModifiedValues(modifierParameters);
        }

        return modifiedValues;
    }
}