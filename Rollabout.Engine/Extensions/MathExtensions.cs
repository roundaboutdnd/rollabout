namespace Rollabout.Engine.Extensions;

internal static class MathExtensions
{
    public static bool Equals(double left, double right) => System.Math.Abs(left - right) == 0;
}