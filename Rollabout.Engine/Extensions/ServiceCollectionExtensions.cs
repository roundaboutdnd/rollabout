using Microsoft.Extensions.DependencyInjection;
using Rollabout.Contracts.Handlers;
using Rollabout.Engine.Handlers;
using Rollabout.Engine.Math;
using Rollabout.Engine.Math.Functions;
using Rollabout.Engine.Modifiers;

namespace Rollabout.Engine.Extensions;

public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Registers services for Rollabout.Engine via dependency injection.
    /// </summary>
    /// <param name="serviceCollection">A collection of service descriptors.</param>
    /// <returns>A collection of service descriptors.</returns>
    public static IServiceCollection AddRollaboutEngineServices(this IServiceCollection serviceCollection)
    {
        // Add modifiers processing
        serviceCollection.AddSingleton<IModifier, CompoundingDiceModifier>();
        serviceCollection.AddSingleton<IModifier, CriticalPointModifier>();
        serviceCollection.AddSingleton<IModifier, DiceMatchingModifier>();
        serviceCollection.AddSingleton<IModifier, ExplodingDiceModifier>();
        serviceCollection.AddSingleton<IModifier, KeepDropDiceModifier>();
        serviceCollection.AddSingleton<IModifier, PenetratingDiceModifier>();
        serviceCollection.AddSingleton<IModifier, RerollingDiceModifier>();
        serviceCollection.AddSingleton<IModifier, SortingDiceModifier>();
        serviceCollection.AddSingleton<IModifier, TargetNumberModifier>();
        serviceCollection.AddSingleton<ModifierFactory>();

        // Add math processing
        serviceCollection.AddSingleton<IFunction, IdentityFunction>();
        serviceCollection.AddSingleton<IFunction, FloorFunction>();
        serviceCollection.AddSingleton<IFunction, RoundFunction>();
        serviceCollection.AddSingleton<IFunction, CeilFunction>();
        serviceCollection.AddSingleton<IFunction, AbsFunction>();
        serviceCollection.AddSingleton<FunctionFactory>();
        serviceCollection.AddSingleton<MathProcessor>();

        // Add handlers
        serviceCollection.AddSingleton<IExpressionHandler, ExpressionHandler>();

        return serviceCollection;
    }
}