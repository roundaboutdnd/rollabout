using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Rollabout.Engine.Modifiers;
using Rollabout.Engine.Options;

namespace Rollabout.Engine.Extensions;

internal static class ModifierExtensions
{
    private static readonly StringBuilder ActionBuilder = new();
    private static readonly StringBuilder CompareOperationBuilder = new();
    private static readonly StringBuilder TargetNumberBuilder = new();
    private static readonly StringBuilder StringBuilder = new();
    private static readonly IEnumerable<string> AvailableActions;
    private static readonly Dictionary<Type, IEnumerable<string>> ModifierTypesWithActions = new();

    private static readonly Dictionary<CompareOperations, string> CompareOperationsMappings = new()
    {
        { CompareOperations.EqualsTo, "=" },
        { CompareOperations.NotEqualsTo, "<>" },
        { CompareOperations.GreaterThan, ">" },
        { CompareOperations.GreaterThanOrEqualsTo, ">=" },
        { CompareOperations.LessThan, "<" },
        { CompareOperations.LessThanOrEqualsTo, "<" }
    };

    static ModifierExtensions()
    {
        var allModifierTypes = AppDomain.CurrentDomain
            .GetAssemblies()
            .SelectMany(a => a.GetTypes())
            .Where(t => t.IsClass && t.IsSubclassOf(typeof(ModifierBase)));

        foreach (var modifierType in allModifierTypes)
        {
            var aliases = modifierType.GetCustomAttribute<ModifierActionsAttribute>()?.Actions;
            if (aliases != null)
            {
                ModifierTypesWithActions.TryAdd(modifierType, aliases);
            }
        }

        AvailableActions = ModifierTypesWithActions.Values.SelectMany(v => v).OrderByDescending(v => v.Length);
    }

    public static IEnumerable<ModifierParameters>? Parse(ReadOnlySpan<char> s)
    {
        StringBuilder.Clear();

        var hasAction = false;
        var modifierParameters = new List<ModifierParameters>();
        foreach (var ch in s)
        {
            if (!hasAction && AvailableActions.Any(a => a.Contains(ch)))
            {
                StringBuilder.Append(ch);
                continue;
            }

            if (char.IsDigit(ch) || CompareOperationsMappings.Values.Any(o => o.Contains(ch)))
            {
                hasAction = true;

                StringBuilder.Append(ch);
                continue;
            }

            var modifierParameter = ParseInternal(StringBuilder.ToString());
            if (modifierParameter != null)
            {
                modifierParameters.Add(modifierParameter);
            }

            hasAction = false;
            StringBuilder.Clear();
            StringBuilder.Append(ch);
        }

        var lastModifierParameter = ParseInternal(StringBuilder.ToString());
        if (lastModifierParameter != null)
        {
            modifierParameters.Add(lastModifierParameter);
        }

        StringBuilder.Clear();

        return modifierParameters.Count == 0
            ? default
            : modifierParameters;
    }

    private static ModifierParameters? ParseInternal(ReadOnlySpan<char> s)
    {
        ActionBuilder.Clear();
        CompareOperationBuilder.Clear();
        TargetNumberBuilder.Clear();

        if (s.IsEmpty || s.IsWhiteSpace())
            return default;

        foreach (var ch in s)
        {
            if (char.IsDigit(ch))
            {
                TargetNumberBuilder.Append(ch);
            }
            else if (DefaultParserOptions.CompareOperationsSymbols.Any(c => ch == c))
            {
                CompareOperationBuilder.Append(ch);
            }
            else
            {
                ActionBuilder.Append(ch);
            }
        }

        var action = ActionBuilder.ToString();
        var compareOperationString = CompareOperationBuilder.ToString();
        var compareOperation = string.IsNullOrEmpty(compareOperationString)
            ? default
            : CompareOperationsMappings.FirstOrDefault(m => m.Value == compareOperationString).Key;

        if (!double.TryParse(TargetNumberBuilder.ToString(), out var targetNumber))
        {
            targetNumber = -1;
        }

        var modifierParameter = new ModifierParameters
        {
            Action = action,
            CompareOperation = compareOperation,
            TargetNumber = MathExtensions.Equals(targetNumber, -1) ? null : targetNumber
        };

        ActionBuilder.Clear();
        CompareOperationBuilder.Clear();
        TargetNumberBuilder.Clear();

        return modifierParameter;
    }
}