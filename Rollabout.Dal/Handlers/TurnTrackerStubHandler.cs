using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Rollabout.Contracts.Entities;
using Rollabout.Contracts.Handlers;

namespace Rollabout.Dal.Handlers;

[Obsolete("This is a stub class for dependency injection purposes only.")]
internal sealed class TurnTrackerStubHandler : ITurnTrackerHandler
{
    public Task<TurnTracker> GetTrackerAsync(long trackerId) => throw new NotImplementedException();

    public Task<TurnTracker?> GetTrackerAsync(ulong chatId, ulong userId) => throw new NotImplementedException();

    public Task<IEnumerable<Turn>> GetTurnsAsync(long trackerId, ulong? userId = default) =>
        throw new NotImplementedException();

    public Task<TurnTrackerStatus?> GetTrackerStatusAsync(long trackerId) => throw new NotImplementedException();

    public Task<TurnTracker> CreateTrackerAsync(TurnTracker tracker) => throw new NotImplementedException();

    public Task<Turn> CreateTurnAsync(Turn turn) => throw new NotImplementedException();

    public Task<TurnTrackerStatus> StartTrackerAsync(long trackerId, ulong userId) =>
        throw new NotImplementedException();

    public Task<TurnTrackerStatus> MoveToNextTurnAsync(long trackerId, ulong userId) =>
        throw new NotImplementedException();

    public Task StopTrackerAsync(long trackerId, ulong userId) =>
        throw new NotImplementedException();

    public Task UpdateTrackerAsync(TurnTracker updatedTracker) => throw new NotImplementedException();

    public Task UpdateTurnAsync(Turn updatedTurn) => throw new NotImplementedException();

    public Task<int> BulkDeleteTurnsAsync(ulong userId, IEnumerable<long> turnsIds) =>
        throw new NotImplementedException();

    public Task DeleteTrackerAsync(long trackerId, ulong userId) => throw new NotImplementedException();

    public Task<int> CleanPrivateTrackersByIntervalAsync(TimeSpan interval) => throw new NotImplementedException();
}