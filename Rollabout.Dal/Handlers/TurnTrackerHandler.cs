using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Rollabout.Contracts.Entities;
using Rollabout.Contracts.Handlers;
using Rollabout.Dal.Entities;
using Rollabout.Dal.Extensions;
using Rollabout.Dal.Repositories;

namespace Rollabout.Dal.Handlers;

internal sealed class TurnTrackerHandler : ITurnTrackerHandler
{
    private readonly ITurnTrackerRepository _turnTrackerRepository;

    public TurnTrackerHandler(ITurnTrackerRepository turnTrackerRepository)
    {
        _turnTrackerRepository = turnTrackerRepository;
    }

    public async Task<TurnTracker> GetTrackerAsync(long trackerId) =>
        (await _turnTrackerRepository.GetTrackerAsync(trackerId)).Map()!;

    public async Task<TurnTracker?> GetTrackerAsync(ulong chatId, ulong userId) =>
        (await _turnTrackerRepository.GetTrackerAsync(chatId, userId)).Map();

    public async Task<IEnumerable<Turn>> GetTurnsAsync(long trackerId, ulong? userId = default) =>
        (await _turnTrackerRepository.GetTurnsAsync(trackerId, userId)).Select(t => t.Map()).Where(t => t != null)!;

    public async Task<TurnTrackerStatus?> GetTrackerStatusAsync(long trackerId) =>
        (await _turnTrackerRepository.GetTrackerStatusAsync(trackerId)).Map();

    public async Task<TurnTracker> CreateTrackerAsync(TurnTracker tracker) =>
        (await _turnTrackerRepository.CreateTrackerAsync(tracker.Map() ?? new TurnTrackerEntity { Id = -1 })).Map()!;

    public async Task<Turn> CreateTurnAsync(Turn turn) =>
        (await _turnTrackerRepository.CreateTurnAsync(turn.Map() ?? new TurnEntity { Id = -1 })).Map()!;

    public async Task<TurnTrackerStatus> StartTrackerAsync(long trackerId, ulong userId) =>
        (await _turnTrackerRepository.StartTrackerAsync(trackerId, userId)).Map()!;

    public async Task<TurnTrackerStatus> MoveToNextTurnAsync(long trackerId, ulong userId) =>
        (await _turnTrackerRepository.MoveToNextTurnAsync(trackerId, userId)).Map()!;

    public Task StopTrackerAsync(long trackerId, ulong userId) =>
        _turnTrackerRepository.StopTrackerAsync(trackerId, userId);

    public Task UpdateTrackerAsync(TurnTracker updatedTracker) =>
        _turnTrackerRepository.UpdateTrackerAsync(updatedTracker.Map() ?? new TurnTrackerEntity { Id = -1 });

    public Task UpdateTurnAsync(Turn updatedTurn) =>
        _turnTrackerRepository.UpdateTurnAsync(updatedTurn.Map() ?? new TurnEntity { Id = -1 });

    public Task<int> BulkDeleteTurnsAsync(ulong userId, IEnumerable<long> turnsIds) =>
        _turnTrackerRepository.BulkDeleteTurnsAsync(userId, turnsIds);

    public Task DeleteTrackerAsync(long trackerId, ulong userId) =>
        _turnTrackerRepository.DeleteTrackerAsync(trackerId, userId);

    public Task<int> CleanPrivateTrackersByIntervalAsync(TimeSpan interval) =>
        _turnTrackerRepository.CleanPrivateTrackersByIntervalAsync(interval);
}