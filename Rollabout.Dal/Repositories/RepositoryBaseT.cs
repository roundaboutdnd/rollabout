using System;
using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Rollabout.Dal.Repositories;

internal abstract class RepositoryBase<T> where T : DbConnection
{
    protected string ConnectionString { get; }

#pragma warning disable CS8618
    protected RepositoryBase()
#pragma warning restore CS8618
    {
    }

    protected RepositoryBase(DatabaseProviderTypes providerType, IConfiguration configuration)
    {
        ConnectionString = configuration.GetConnectionString(providerType.ToString()) ?? string.Empty;

        if (string.IsNullOrEmpty(ConnectionString))
            throw new ArgumentNullException(nameof(ConnectionString));

        // ReSharper disable once VirtualMemberCallInConstructor
        Setup();
    }

    protected async Task<TResult> ExecuteQueryAsync<TResult>(Func<DbConnection, Task<TResult>> queryFunc)
    {
        await using var connection = Activator.CreateInstance(typeof(T), ConnectionString) as T;
        if (connection == null)
            throw new InvalidOperationException();

        await connection.OpenAsync();
        TResult result;
        try
        {
            result = await queryFunc.Invoke(connection);
        }
        catch
        {
            await connection.CloseAsync();
            throw;
        }

        await connection.CloseAsync();
        return result;
    }

    protected async Task<TResult> ExecuteTransactionAsync<TResult>(
        Func<DbConnection, DbTransaction, Task<TResult>> transactionFunc)
    {
        await using var connection = Activator.CreateInstance(typeof(T), ConnectionString) as T;
        if (connection == null)
            throw new InvalidOperationException();

        await connection.OpenAsync();
        await using var transaction = await connection.BeginTransactionAsync();
        TResult result;
        try
        {
            result = await transactionFunc.Invoke(connection, transaction);
        }
        catch
        {
            await transaction.RollbackAsync();
            await connection.CloseAsync();
            throw;
        }

        await transaction.CommitAsync();
        await connection.CloseAsync();
        return result;
    }

    protected abstract void Setup();
}