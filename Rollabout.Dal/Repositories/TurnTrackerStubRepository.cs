using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using Rollabout.Dal.Entities;

namespace Rollabout.Dal.Repositories;

[Obsolete("This is a stub class for dependency injection purposes only.")]
internal sealed class TurnTrackerStubRepository : RepositoryBase<DbConnection>, ITurnTrackerRepository
{
    public Task<TurnTrackerEntity> GetTrackerAsync(long trackerId) => throw new NotImplementedException();

    public Task<TurnTrackerEntity?> GetTrackerAsync(ulong chatId, ulong userId) => throw new NotImplementedException();

    public Task<IEnumerable<TurnEntity>> GetTurnsAsync(long trackerId, ulong? userId = default) =>
        throw new NotImplementedException();

    public Task<TurnTrackerStatusEntity?> GetTrackerStatusAsync(long trackerId) => throw new NotImplementedException();

    public Task<TurnTrackerEntity> CreateTrackerAsync(TurnTrackerEntity trackerEntity) =>
        throw new NotImplementedException();

    public Task<TurnEntity> CreateTurnAsync(TurnEntity turnEntity) => throw new NotImplementedException();

    public Task<TurnTrackerStatusEntity> StartTrackerAsync(long trackerId, ulong userId) =>
        throw new NotImplementedException();

    public Task<TurnTrackerStatusEntity> MoveToNextTurnAsync(long trackerId, ulong userId) =>
        throw new NotImplementedException();

    public Task StopTrackerAsync(long trackerId, ulong userId) => throw new NotImplementedException();

    public Task UpdateTrackerAsync(TurnTrackerEntity trackerEntity) => throw new NotImplementedException();

    public Task UpdateTurnAsync(TurnEntity updatedTurnEntity) => throw new NotImplementedException();

    public Task<int> BulkDeleteTurnsAsync(ulong userId, IEnumerable<long> turnsIds) =>
        throw new NotImplementedException();

    public Task DeleteTrackerAsync(long trackerId, ulong userId) => throw new NotImplementedException();

    public Task<int> CleanPrivateTrackersByIntervalAsync(TimeSpan interval) => throw new NotImplementedException();

    protected override void Setup() => throw new NotImplementedException();
}