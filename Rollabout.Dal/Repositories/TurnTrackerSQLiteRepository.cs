using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Configuration;
using Rollabout.Contracts.Exceptions;
using Rollabout.Dal.Entities;
using UnauthorizedAccessException = Rollabout.Contracts.Exceptions.UnauthorizedAccessException;

namespace Rollabout.Dal.Repositories;

// ReSharper disable InconsistentNaming
internal sealed class TurnTrackerSQLiteRepository : RepositoryBase<SqliteConnection>, ITurnTrackerRepository
{
    public TurnTrackerSQLiteRepository(IConfiguration configuration)
        : base(DatabaseProviderTypes.SQLite, configuration)
    {
    }

    public async Task<TurnTrackerEntity> GetTrackerAsync(long trackerId) =>
        await ExecuteQueryAsync(c => c.QueryFirstOrDefaultAsync<TurnTrackerEntity>(
            "SELECT * FROM trackers WHERE Id = @trackerId;", new { trackerId }
        ));

    public async Task<TurnTrackerEntity?> GetTrackerAsync(ulong chatId, ulong userId) =>
        await ExecuteQueryAsync(c => c.QueryFirstOrDefaultAsync<TurnTrackerEntity?>(
            "SELECT * FROM trackers WHERE ChatId = @chatId AND UserId = @userId AND Private IS FALSE;",
            new
            {
                chatId = chatId.ToString(),
                userId = userId.ToString()
            }
        ));

    public async Task<IEnumerable<TurnEntity>> GetTurnsAsync(long trackerId, ulong? userId = default) =>
        await ExecuteQueryAsync(c =>
            userId.HasValue
                ? c.QueryAsync<TurnEntity>(
                    "SELECT * FROM turns WHERE TrackerId = @trackerId AND UserId = @userId;",
                    new
                    {
                        trackerId,
                        userId = userId.ToString()
                    })
                : c.QueryAsync<TurnEntity>(
                    "SELECT * FROM turns WHERE TrackerId = @trackerId;",
                    new { trackerId })
        );

    public async Task<TurnTrackerStatusEntity?> GetTrackerStatusAsync(long trackerId) =>
        await ExecuteTransactionAsync(async (c, t) =>
        {
            var tracker = await c.QueryFirstAsync<TurnTrackerEntity>("SELECT * FROM trackers WHERE Id = @trackerId;",
                new { trackerId }, t);

            var currentTurn = await c.QueryFirstOrDefaultAsync<TurnEntity?>(
                "SELECT * FROM turns WHERE Id = @currentTurnId;",
                new { currentTurnId = tracker.CurrentTurnId }, t);

            var nextTurnId = tracker.NextTurnId;
            if (currentTurn == null)
            {
                nextTurnId = await c.QueryFirstOrDefaultAsync<long?>(
                    "SELECT NextTurnId FROM trackers WHERE Id = @trackerId;", new { trackerId }, t);

                if (nextTurnId.HasValue)
                {
                    currentTurn = await c.QueryFirstAsync<TurnEntity>(
                        "SELECT * FROM turns WHERE Id = @nextTurnId;", new { nextTurnId }, t);
                }
            }

            if (currentTurn?.Id == nextTurnId)
            {
                nextTurnId = -1;
            }

            var nextTurn =
                (await c.QueryFirstOrDefaultAsync<TurnEntity?>("SELECT * FROM turns WHERE Id = @nextTurnId;",
                    new { nextTurnId }, t) ?? await c.QueryFirstOrDefaultAsync<TurnEntity?>(
                    "SELECT * FROM turns WHERE TrackerId = @trackerId AND Value < @maxValue ORDER BY Value DESC LIMIT 1;",
                    new
                    {
                        trackerId,
                        maxValue = currentTurn?.Value ?? -1
                    }, t)) ?? await c.QueryFirstOrDefaultAsync<TurnEntity?>(
                    "SELECT * FROM turns WHERE TrackerId = @trackerId ORDER BY Value DESC LIMIT 1;",
                    new { trackerId }, t);

            if (currentTurn == null || currentTurn.Id == nextTurn?.Id)
            {
                currentTurn = null;
                nextTurn = null;
            }

            await c.ExecuteAsync(
                "UPDATE trackers SET CurrentTurnId = @currentTurnId, NextTurnId = @nextTurnId WHERE Id = @trackerId;",
                new
                {
                    currentTurnId = currentTurn?.Id ?? null,
                    nextTurnId = nextTurn?.Id ?? null,
                    trackerId
                }, t);

            return new TurnTrackerStatusEntity
            {
                CurrentTurn = currentTurn,
                NextTurn = nextTurn
            };
        });

    public async Task<TurnTrackerEntity> CreateTrackerAsync(TurnTrackerEntity trackerEntity) =>
        await ExecuteQueryAsync(c => c.QuerySingleAsync<TurnTrackerEntity>(
            "INSERT INTO trackers (ChatId, UserId, MessageId, Private) VALUES (@chatId, @userId, @messageId, @isPrivate) RETURNING *;",
            new
            {
                chatId = trackerEntity.ChatId,
                userId = trackerEntity.UserId,
                messageId = trackerEntity.MessageId,
                isPrivate = trackerEntity.Private
            }
        ));

    public async Task<TurnEntity> CreateTurnAsync(TurnEntity turnEntity) =>
        await ExecuteTransactionAsync(async (c, t) =>
        {
            var newTurn = await c.QuerySingleAsync<TurnEntity>(
                "INSERT INTO turns (TrackerId, UserId, Name, Value) VALUES (@trackerId, @userId, @name, @value) RETURNING *;",
                new
                {
                    trackerId = turnEntity.TrackerId,
                    userId = turnEntity.UserId,
                    name = turnEntity.Name,
                    value = turnEntity.Value
                }, t);

            var nextTurn = await c.QueryFirstOrDefaultAsync<TurnEntity?>(
                "SELECT * FROM turns WHERE Id = (SELECT NextTurnId FROM trackers WHERE Id = @trackerId);",
                new { trackerId = turnEntity.TrackerId }, t);

            if (nextTurn != null && nextTurn.Value < newTurn.Value)
            {
                var turns = await c.QueryAsync<TurnEntity?>(
                    "SELECT * FROM turns WHERE TrackerId = @trackerId ORDER BY Value DESC LIMIT 2;",
                    new { trackerId = turnEntity.TrackerId }, t);

                if (turns.Any(e => e?.Value < nextTurn.Value))
                {
                    await c.ExecuteAsync("UPDATE trackers SET NextTurnId = @nextTurnId WHERE Id = @trackerId;",
                        new
                        {
                            nextTurnId = newTurn.Id,
                            trackerId = turnEntity.TrackerId
                        }, t);
                }
            }

            return newTurn;
        });

    public async Task<TurnTrackerStatusEntity> StartTrackerAsync(long trackerId, ulong userId) =>
        await ExecuteTransactionAsync(async (c, t) =>
        {
            if (!await CheckEntityCreator(typeof(TurnTrackerEntity), trackerId, userId, c, t))
                throw new UnauthorizedAccessException(userId);

            var turns = (await c.QueryAsync<TurnEntity>(
                "SELECT * FROM turns WHERE TrackerId = @trackerId ORDER BY VALUE DESC LIMIT 2;",
                new { trackerId }, t))?.ToArray();

            if (turns is not { Length: 2 })
                throw new StartTrackerException(trackerId);

            await c.ExecuteAsync(
                "UPDATE trackers SET CurrentTurnId = @currentTurnId, NextTurnId = @nextTurnId WHERE Id = @trackerId;",
                new
                {
                    currentTurnId = turns[0].Id,
                    nextTurnId = turns[1].Id,
                    trackerId
                }, t);

            return new TurnTrackerStatusEntity
            {
                CurrentTurn = turns[0],
                NextTurn = turns[1]
            };
        });

    public async Task<TurnTrackerStatusEntity> MoveToNextTurnAsync(long trackerId, ulong userId) =>
        await ExecuteTransactionAsync(async (c, t) =>
        {
            if (!await CheckEntityCreator(typeof(TurnTrackerEntity), trackerId, userId, c, t))
                throw new UnauthorizedAccessException(userId);

            var currentTurnId = await c.QueryFirstOrDefaultAsync<long?>(
                "SELECT CurrentTurnId FROM trackers WHERE Id = @trackerId;", new { trackerId }, t);

            if (!currentTurnId.HasValue)
                throw new InactiveTrackerException(trackerId);

            var maxValue = await c.QueryFirstAsync<double>(
                "SELECT Value FROM turns WHERE Id = @currentTurnId;", new { currentTurnId }, t);

            var turns = (await c.QueryAsync<TurnEntity>(
                "SELECT * FROM turns WHERE TrackerId = @trackerId AND Value < @maxValue ORDER BY Value DESC LIMIT 2;",
                new { trackerId, maxValue }, t)).ToList();

            switch (turns.Count)
            {
                case 1:
                {
                    turns.Add(await c.QueryFirstAsync<TurnEntity>(
                        "SELECT * FROM turns WHERE TrackerId = @trackerId ORDER BY Value DESC;",
                        new { trackerId }, t));
                    break;
                }
                case 0:
                {
                    turns.AddRange(await c.QueryAsync<TurnEntity>(
                        "SELECT * FROM turns WHERE TrackerId = @trackerId ORDER BY Value DESC LIMIT 2;",
                        new { trackerId }, t) ?? Enumerable.Empty<TurnEntity>());
                    break;
                }
            }

            await c.ExecuteAsync(
                "UPDATE trackers SET CurrentTurnId = @currentTurnId, NextTurnId = @nextTurnId WHERE Id = @trackerId;",
                new
                {
                    currentTurnId = turns[0].Id,
                    nextTurnId = turns[1].Id,
                    trackerId
                }, t);

            return new TurnTrackerStatusEntity
            {
                CurrentTurn = turns[0],
                NextTurn = turns[1]
            };
        });

    public async Task StopTrackerAsync(long trackerId, ulong userId) => await ExecuteTransactionAsync(async (c, t) =>
    {
        if (!await CheckEntityCreator(typeof(TurnTrackerEntity), trackerId, userId, c, t))
            throw new UnauthorizedAccessException(userId);

        var tracker = await c.QuerySingleAsync<TurnTrackerEntity>(
            "SELECT * FROM trackers WHERE Id = @trackerId;", new { trackerId }, t);

        if (!tracker.CurrentTurnId.HasValue && !tracker.NextTurnId.HasValue)
            throw new InactiveTrackerException(trackerId);

        return c.ExecuteAsync(
            "UPDATE trackers SET CurrentTurnId = DEFAULT, NextTurnId = DEFAULT WHERE Id = @trackerId;",
            new { trackerId }, t);
    });

    public async Task UpdateTrackerAsync(TurnTrackerEntity trackerEntity) =>
        await ExecuteTransactionAsync(async (c, t) =>
        {
            if (!await CheckEntityCreator(typeof(TurnTrackerEntity), trackerEntity.Id, trackerEntity.UserId, c, t))
                throw new UnauthorizedAccessException(trackerEntity.UserId);

            return c.ExecuteAsync(
                "UPDATE trackers SET CurrentTurnId = ifnull(@currentTurnId, CurrentTurnId), NextTurnId = ifnull(@nextTurnId, NextTurnId), MessageId = ifnull(@messageId, MessageId) WHERE Id = @trackerId;",
                new
                {
                    currentTurnId = trackerEntity.CurrentTurnId,
                    nextTurnId = trackerEntity.NextTurnId,
                    messageId = trackerEntity.MessageId,
                    trackerId = trackerEntity.Id
                }, t);
        });

    public async Task UpdateTurnAsync(TurnEntity updatedTurnEntity) =>
        await ExecuteTransactionAsync(async (c, t) =>
        {
            if (!await CheckEntityCreator(typeof(TurnEntity), updatedTurnEntity.Id, updatedTurnEntity.UserId, c, t) &&
                !await CheckEntityCreator(typeof(TurnTrackerEntity), updatedTurnEntity.TrackerId,
                    updatedTurnEntity.UserId, c, t))
                throw new UnauthorizedAccessException(updatedTurnEntity.UserId);

            return c.ExecuteAsync(
                "UPDATE turns SET Name = ifnull(@name, Name), Value = ifnull(@value, Value) WHERE Id = @turnId;",
                new
                {
                    turnId = updatedTurnEntity.Id,
                    name = updatedTurnEntity.Name,
                    value = updatedTurnEntity.Value
                }, t);
        });

    public async Task<int> BulkDeleteTurnsAsync(ulong userId, IEnumerable<long> turnsIds) =>
        await ExecuteTransactionAsync(async (c, t) =>
        {
            var turnIdsList = turnsIds.ToArray();
            var turns = (await c.QueryAsync<TurnEntity>(
                "SELECT * FROM turns WHERE Id IN @turnIds;", new { turnIds = turnIdsList },
                t)).ToArray();

            if (turns.Any(x => !ulong.TryParse(x.UserId, out var creatorId) || creatorId != userId))
            {
                var trackerIds = turns.Select(x => x.TrackerId).ToArray();
                var creatorIds = (await c.QueryAsync<string>(
                    "SELECT DISTINCT UserId FROM trackers WHERE Id IN @trackerIds;",
                    new { trackerIds }, t)).ToArray();

                if (creatorIds is not { Length: 1 })
                    throw new UnauthorizedAccessException(userId);
            }

            return await c.ExecuteAsync("DELETE FROM turns WHERE Id IN @turnIds; SELECT changes();",
                new { turnIds = turnIdsList }, t);
        });

    public async Task DeleteTrackerAsync(long trackerId, ulong userId) => await ExecuteTransactionAsync(async (c, t) =>
    {
        if (!await CheckEntityCreator(typeof(TurnTrackerEntity), trackerId, userId, c, t))
            throw new UnauthorizedAccessException(userId);

        return c.ExecuteAsync("DELETE FROM trackers WHERE Id = @trackerId;", new { trackerId }, t);
    });

    public async Task<int> CleanPrivateTrackersByIntervalAsync(TimeSpan interval) =>
        await ExecuteQueryAsync(async c => await c.ExecuteAsync(
            "DELETE FROM trackers WHERE Private IS TRUE AND CreateTimestamp < @maxTimestamp; SELECT changes();",
            new { maxTimestamp = DateTime.UtcNow.Subtract(interval) }));

    protected override async void Setup()
    {
        var databasePath = Directory.GetParent(ConnectionString.Replace("Data Source=", string.Empty).Trim(';'));
        if (databasePath?.Exists == false)
        {
            databasePath.Create();
        }

        await ExecuteTransactionAsync(async (c, t) =>
        {
            await c.ExecuteAsync(
                "CREATE TABLE IF NOT EXISTS trackers (Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ChatId TEXT NOT NULL, UserId TEXT NOT NULL, MessageId TEXT NULL, CurrentTurnId INTEGER NULL, NextTurnId INTEGER NULL, Private BOOLEAN NOT NULL, CreateTimestamp DATETIME DEFAULT (DATETIME('now')) NOT NULL);",
                transaction: t);

            await c.ExecuteAsync(
                "CREATE UNIQUE INDEX IF NOT EXISTS chat_can_have_only_one_public_tracker_per_user ON trackers (ChatId, UserId) WHERE (Private IS 0);",
                transaction: t);

            return await c.ExecuteAsync(
                "CREATE TABLE IF NOT EXISTS turns (Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, TrackerId INTEGER REFERENCES trackers (Id) ON DELETE CASCADE NOT NULL, UserId TEXT NOT NULL, Name TEXT NOT NULL, Value REAL NOT NULL, CONSTRAINT turn_name_must_be_unique_for_tracker UNIQUE (TrackerId, Name) ON CONFLICT ABORT);",
                transaction: t);
        });
    }

    private static async Task<bool> CheckEntityCreator(Type entityType, long entityId, ulong userId,
        IDbConnection connection, IDbTransaction? transaction = default)
    {
        var tableName = string.Empty;
        if (entityType == typeof(TurnTrackerEntity))
        {
            tableName = "trackers";
        }
        else if (entityType == typeof(TurnEntity))
        {
            tableName = "turns";
        }

        var creatorIdString = await connection.QueryFirstOrDefaultAsync<string?>(
            $"SELECT UserId FROM {tableName} WHERE Id = @entityId;", new { entityId }, transaction);

        return ulong.TryParse(creatorIdString, out var creatorId) && userId == creatorId;
    }

    private static async Task<bool> CheckEntityCreator(Type entityType, long entityId, string userId,
        IDbConnection connection, IDbTransaction? transaction = default)
    {
        var tableName = string.Empty;
        if (entityType == typeof(TurnTrackerEntity))
        {
            tableName = "trackers";
        }
        else if (entityType == typeof(TurnEntity))
        {
            tableName = "turns";
        }

        var creatorId = await connection.QueryFirstOrDefaultAsync<string?>(
            $"SELECT UserId FROM {tableName} WHERE Id = @entityId;", new { entityId }, transaction);

        return string.Equals(userId, creatorId);
    }
}