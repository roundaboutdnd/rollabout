using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Rollabout.Dal.Entities;

namespace Rollabout.Dal.Repositories;

internal interface ITurnTrackerRepository
{
    Task<TurnTrackerEntity> GetTrackerAsync(long trackerId);

    Task<TurnTrackerEntity?> GetTrackerAsync(ulong chatId, ulong userId);

    Task<IEnumerable<TurnEntity>> GetTurnsAsync(long trackerId, ulong? userId = default);

    Task<TurnTrackerStatusEntity?> GetTrackerStatusAsync(long trackerId);

    Task<TurnTrackerEntity> CreateTrackerAsync(TurnTrackerEntity trackerEntity);

    Task<TurnEntity> CreateTurnAsync(TurnEntity turnEntity);

    Task<TurnTrackerStatusEntity> StartTrackerAsync(long trackerId, ulong userId);

    Task<TurnTrackerStatusEntity> MoveToNextTurnAsync(long trackerId, ulong userId);

    Task StopTrackerAsync(long trackerId, ulong userId);

    Task UpdateTrackerAsync(TurnTrackerEntity trackerEntity);

    Task UpdateTurnAsync(TurnEntity updatedTurnEntity);

    Task<int> BulkDeleteTurnsAsync(ulong userId, IEnumerable<long> turnsIds);

    Task DeleteTrackerAsync(long trackerId, ulong userId);

    Task<int> CleanPrivateTrackersByIntervalAsync(TimeSpan interval);
}