using Rollabout.Contracts.Entities;
using Rollabout.Dal.Entities;

namespace Rollabout.Dal.Extensions;

internal static class MappingExtensions
{
    public static TurnEntity? Map(this Turn? turn) => turn == null
        ? default
        : new TurnEntity
        {
            Id = turn.Id,
            TrackerId = turn.TrackerId,
            UserId = turn.UserId.ToString(),
            Name = turn.Name,
            Value = turn.Value
        };

    public static TurnTracker? Map(this TurnTrackerEntity? entity, TurnEntity? currentTurnEntity = default,
        TurnEntity? nextTurnEntity = default)
    {
        if (entity == null)
            return default;

        if (!ulong.TryParse(entity.MessageId, out var messageId))
        {
            messageId = 0;
        }

        return new TurnTracker
        {
            Id = entity.Id,
            ChatId = ulong.Parse(entity.ChatId),
            UserId = ulong.Parse(entity.UserId),
            CurrentTurnId = currentTurnEntity?.Id,
            NextTurnId = nextTurnEntity?.Id,
            Private = entity.Private,
            MessageId = messageId == 0
                ? default
                : messageId
        };
    }

    public static TurnTrackerEntity? Map(this TurnTracker? tracker) => tracker == null
        ? default
        : new TurnTrackerEntity
        {
            Id = tracker.Id,
            ChatId = tracker.ChatId.ToString(),
            UserId = tracker.UserId.ToString(),
            CurrentTurnId = tracker.CurrentTurnId,
            NextTurnId = tracker.NextTurnId,
            Private = tracker.Private,
            MessageId = tracker.MessageId?.ToString()
        };

    public static Turn? Map(this TurnEntity? entity) => entity == null
        ? default
        : new Turn
        {
            Id = entity.Id,
            TrackerId = entity.TrackerId,
            UserId = ulong.Parse(entity.UserId),
            Name = entity.Name,
            Value = entity.Value
        };

    public static TurnTrackerStatus? Map(this TurnTrackerStatusEntity? entity) => entity == null
        ? default
        : new TurnTrackerStatus
        {
            CurrentTurn = entity.CurrentTurn.Map(),
            NextTurn = entity.NextTurn.Map()
        };
}