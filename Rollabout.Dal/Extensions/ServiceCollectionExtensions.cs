using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Rollabout.Contracts.Handlers;
using Rollabout.Dal.Handlers;
using Rollabout.Dal.Repositories;
using Rollabout.Dal.Services;

#pragma warning disable CS0618

namespace Rollabout.Dal.Extensions;

public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Registers database services for Rollabout.Engine via dependency injection.
    /// </summary>
    /// <param name="serviceCollection">A collection of service descriptors.</param>
    /// <param name="configuration">A set of key/value application configuration properties.</param>
    /// <returns>A collection of service descriptors.</returns>
    public static IServiceCollection AddRollaboutDatabaseServices(this IServiceCollection serviceCollection,
        IConfiguration configuration)
    {
        serviceCollection
            .AddOptions<DatabaseOptions>()
            .Bind(configuration.GetSection(DatabaseOptions.ConfigurationSectionName));

        var hasConnectionStrings = configuration.GetSection("ConnectionStrings").GetChildren().Any();

        serviceCollection.AddSingleton<ITurnTrackerRepository>(_ =>
        {
            var connectionString = configuration.GetConnectionString(DatabaseProviderTypes.PostgreSQL.ToString());
            if (!string.IsNullOrEmpty(connectionString))
                return new TurnTrackerPostgreSQLRepository(configuration);

            connectionString = configuration.GetConnectionString(DatabaseProviderTypes.SQLite.ToString());

            return string.IsNullOrEmpty(connectionString)
                ? new TurnTrackerStubRepository()
                : new TurnTrackerSQLiteRepository(configuration);
        });

        serviceCollection.AddSingleton<ITurnTrackerHandler>(p => hasConnectionStrings
            ? new TurnTrackerHandler(p.GetRequiredService<ITurnTrackerRepository>())
            : new TurnTrackerStubHandler());

        if (hasConnectionStrings)
        {
            var connectionString = configuration.GetConnectionString(DatabaseProviderTypes.PostgreSQL.ToString());
            if (!string.IsNullOrEmpty(connectionString))
            {
                serviceCollection.AddHealthChecks().AddNpgSql(connectionString, name: "db");
            }
            else
            {
                connectionString = configuration.GetConnectionString(DatabaseProviderTypes.SQLite.ToString());

                if (!string.IsNullOrEmpty(connectionString))
                {
                    serviceCollection.AddHealthChecks().AddSqlite(connectionString, name: "db");
                }
            }
        }

        serviceCollection.AddHostedService<BackgroundService>(p =>
        {
            var options = p.GetService<IOptions<DatabaseOptions>>();
            var databaseOptions = options?.Value;
            return databaseOptions == null || databaseOptions.CleanupInterval.HasValue == false ||
                   databaseOptions.CleanPrivateTrackersInterval.HasValue == false ||
                   !hasConnectionStrings
                ? new CleanupStubBackgroundService()
                : new CleanupBackgroundService(p.GetRequiredService<ITurnTrackerHandler>(), options!,
                    p.GetRequiredService<ILogger<CleanupBackgroundService>>());
        });

        return serviceCollection;
    }
}