// ReSharper disable InconsistentNaming

namespace Rollabout.Dal;

/// <summary>
/// Available database provider types.
/// </summary>
public enum DatabaseProviderTypes
{
    /// <summary>
    /// No database provider.
    /// </summary>
    None,

    /// <summary>
    /// PostgreSQL database provider.
    /// </summary>
    PostgreSQL,

    /// <summary>
    /// SQLite database provider.
    /// </summary>
    SQLite
}