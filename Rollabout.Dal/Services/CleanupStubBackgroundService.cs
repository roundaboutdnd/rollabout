using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace Rollabout.Dal.Services;

[Obsolete("This is a stub class for dependency injection purposes only.")]
internal sealed class CleanupStubBackgroundService : BackgroundService
{
    protected override Task ExecuteAsync(CancellationToken stoppingToken) => Task.CompletedTask;
}