using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Rollabout.Contracts.Handlers;

namespace Rollabout.Dal.Services;

internal sealed class CleanupBackgroundService : BackgroundService
{
    private readonly ITurnTrackerHandler _turnTrackerHandler;
    private readonly DatabaseOptions _databaseOptions;
    private readonly ILogger<CleanupBackgroundService> _logger;

    public CleanupBackgroundService(ITurnTrackerHandler turnTrackerHandler, IOptions<DatabaseOptions> databaseOptions,
        ILogger<CleanupBackgroundService> logger)
    {
        _turnTrackerHandler = turnTrackerHandler;
        _databaseOptions = databaseOptions.Value;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            _logger.LogInformation("Beginning scheduled cleanup...");

            try
            {
                var deletedCount = await _turnTrackerHandler.CleanPrivateTrackersByIntervalAsync(
                    _databaseOptions.CleanPrivateTrackersInterval!.Value);

                _logger.LogInformation("Successfully deleted {DeletedCount} private trackers", deletedCount);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occured during cleanup: {ExMessage}", ex.Message);
            }

            await Task.Delay(_databaseOptions.CleanupInterval!.Value, stoppingToken);
        }
    }
}