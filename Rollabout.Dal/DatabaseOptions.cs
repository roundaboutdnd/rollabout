using System;

namespace Rollabout.Dal;

/// <summary>
/// Database configuration.
/// </summary>
public sealed class DatabaseOptions
{
    /// <summary>
    /// Configuration section name.
    /// </summary>
    public static string ConfigurationSectionName => "Database";

    /// <summary>
    /// Private turn trackers lifetime.
    /// </summary>
    public TimeSpan? CleanPrivateTrackersInterval { get; set; } = TimeSpan.FromHours(12);

    /// <summary>
    /// How often to start cleanup service.
    /// </summary>
    public TimeSpan? CleanupInterval { get; set; } = TimeSpan.FromHours(1);
}