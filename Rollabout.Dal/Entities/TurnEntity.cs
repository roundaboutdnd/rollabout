namespace Rollabout.Dal.Entities;

internal sealed record TurnEntity
{
    public long Id { get; init; }

    public long TrackerId { get; init; }

    public string UserId { get; init; } = null!;

    public string? Name { get; init; }

    public double? Value { get; init; }
}