namespace Rollabout.Dal.Entities;

internal sealed record TurnTrackerStatusEntity
{
    public TurnEntity? CurrentTurn { get; init; }

    public TurnEntity? NextTurn { get; init; }
}