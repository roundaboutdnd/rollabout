namespace Rollabout.Dal.Entities;

internal sealed record TurnTrackerEntity
{
    public long Id { get; init; }

    public string ChatId { get; init; } = null!;

    public string UserId { get; init; } = null!;

    public string? MessageId { get; init; }

    public long? CurrentTurnId { get; init; }

    public long? NextTurnId { get; init; }

    public bool Private { get; init; }
}